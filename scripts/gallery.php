<?php
// apt-get install php-imagick
// php -m | grep imagick

// This file is designed to return image data to your AJAX call. It will attempt to resize the image to
// the largest size the original image can allow. I can't make your image larger than the original.

// File expects a URL encoded variable string with the following (keys: values):

//   width: integer - maximum desired width
//  height: integer - maximum desired height
// gallery: string <= 255 characters - gallery's folder name
//  picLoc: string <= 255 characters - picture file name

// This file will return:

// 1. width of new image
// 2. height of new image
// 3. base64 encoded image data

// the above will be returned in a string with a deliminator of _&_

// $_POST['functToGet'] = 'imageGet';
if(@$_POST['functToGet'] == 'imageGet'){

    // ** sanitize the variables. Terminate script if requirements unmet ** \\

    if(@$_POST['width'] !== '' && is_int((int)@$_POST['width'])){
        // user passed the width and it is an integer
        $wantWidth = (int)$_POST['width']; // set width wanted integer
    }else{
        die(); // exit without comment
    }

    if(@$_POST['height'] !== '' && is_int((int)@$_POST['height'])){
        // user passed the height and it is an integer
        $wantHeight = (int)$_POST['height']; // set height wanted integer
    }else{
        die(); // exit without comment
    }

    if(is_string(@$_POST['gallery']) && @$_POST['gallery'] !== '' && strlen(@$_POST['gallery']) < 256){
        // user has passed the gallery filename. Is String and is less than 256 characters long.
        $galleryName = $_POST['gallery']; // set our gallery name
    }else{
        die(); // exit without comment
    }

    if(is_string(@$_POST['picLoc']) && @$_POST['picLoc'] !== ''){
        
        $imgLoc = $_POST['picLoc']; // set our picture name

        if(strpos($imgLoc, '/')){
            // We really only need the filename not a file path and I think this is a file path
            $imgLoc = explode('/', $imgLoc)[1]; // set our picture name to the end of the file path
            if(strlen($imgLoc) > 255){
                // String is greater than 256 characters long.
                die();
            }

        }elseif(strlen(@$_POST['picLoc']) > 255){
            // String is greater than 256 characters long.
            die();
        }

    }else{
        die(); // exit without comment
    }
    // _________________________________________________________

    // ** Set up our original image file location ** \\

    // We need to find our current working directory's absolute path. 
    $loc = explode('/', __FILE__);

    // we need to basically step out to our parent directory. ie: ../
    $loc = array_slice($loc, 0, count($loc) - 2);

    // Our largest images are in the original archive folder separated by gallery name
    foreach([$galleryName, 'Original_Archive', $imgLoc] as $val){
        // We are building our image file path sequentially
        array_push($loc, $val);
    }

    // Join our images absolute file path into a posix path string
    $loc = implode($loc, '/');
    // _________________________________________________________

    // ** Resize our image ** \\

    // find our maximum image dimensions based on the what the user asked for previously above. 
    // We also need our image mime type.
    list($width, $height, $mime) = imgRX($loc, $wantWidth, $wantHeight);

    $img = new Imagick($loc); // create new imagick image based on our Original_Archive image
    $img->resizeImage($width, $height, imagick::COMPRESSION_NO, 1); // resize our image 
    $imgBlob = base64_encode($img->getimageblob()); // encode our image blob base64 for data
    $img->clear(); // clear our image data.
    $img->destroy(); // destroy it

    // _________________________________________________________

    // ** Return our width, height and base64 data separated by a hopefully unique enough deliminator.
    echo $width.'_&_'.$height.'_&_'.'data:'.$mime.';base64,'.$imgBlob;
}
else{
    // the user has failed our basic (what) question
    die(); // exit without comment
}

// Here is the image resize function
function imgRX($imgPath, $maxWidth, $maxHeight) {
    // Get width and height of original image
    $size = getimagesize($imgPath);
    if($size === FALSE) return FALSE;

    $resizedWidth = $size[0];
    $resizedHeight = $size[1];

    if($resizedWidth > $maxWidth){
        $aspectRatio = $maxWidth / $resizedWidth;
        $resizedWidth = round($aspectRatio * $resizedWidth);
        $resizedHeight = round($aspectRatio * $resizedHeight);
    }
    if($resizedHeight > $maxHeight){
        $aspectRatio = $maxHeight / $resizedHeight;
        $resizedWidth = round($aspectRatio * $resizedWidth);
        $resizedHeight = round($aspectRatio * $resizedHeight);
    }

    return array($resizedWidth, $resizedHeight, $size['mime']);
}

?>
