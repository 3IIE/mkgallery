
#    mkGallery.py


This file will build and maintain the parts and peaces one needs for a
functional html image gallery. As of yet it only handles the basics. Here's a 
list.

1. Backs up original files to a folder created called Original_Archive.

       * This file will use this original when resized and as such is the maximum
   image resolution. The fullscreen will use these images to dynamically
   load the larges image possible through server side php.

2. Creates the necessary images and thumbnails in their respective 
   directories.

3. Creates and maintains the gallery's json object.

       * The json object will hold the maximum hight and width for the gallery
   images and thumbnails. This means that once you have set your 
   gallery's image and thumb size you only have to pass the size
   parameters when you wish to change the size of all images in your 
   gallery. 

       * The json file will also hold all your gallery's and images.

       * Adding new images is as easy as pasting your image in the correct
   gallery you want and naming the file what you want the filename to
   be displayed as in the about image section and running:

```bash
      $ python3 mkGallery.py
```

4. Creates all the web files. HTML, javascript, CSS and PHP in a scripts
   directory.

       * You can create all the web files by passing the -all flag or build 
   individually by passing one or more of your desired file creation flags.


    1.       HTML: -html 
    2. javascript: -js
    3.        php: -php
    4.        css: -css

  #Examples:

```bash
# make all files
$ python3 mkGallery.py -all

# make html and javascript
$ python3 mkGallery.py -html -js
```

5. Gives the user basic helper functions to keep their gallery clean and
   current.

## Install:

1. Download the mkGallery.py file.

2. Move file to gallery location.

3. Install Pillow (Python Image Library, PIL):

  * Check if Pillow is installed on your machine. I have version 8.1.0 as shown 
  below. I have found versions >= 8.1.0 have the best image quality.

```bash
$ pip freeze | grep Pillow
Pillow==8.1.0
```

  * Install Pillow:
   
```bash
$ pip install Pillow
```

## Basic Usage:

###    Building Gallery's:

1. Create and name your gallery's image directory. Or many if you want.

2. Move images into gallery's and name the images. This will support
   naming with spaces and with underscore as a word deliminator instead of 

3. Move the mkGallery.py file to the same directory your gallery folders
   are in. It is important here to note that mkGallery.py will search
   literally every folder that is in the same directory as it is in. It
   will make backups, images, thumbnails and links in json to EVERY 
   image file it finds. It will do this recursively. 

       * As such I advise you designate mkGallery.py and its gallery's it's
   own folder. If you must keep other images with your gallery's you 
   can keep them in hidden .directories. (see hidden files in \*nix for
   further details)

4. Once you have your images in their gallery setup the way you want you
   can now build gallery and json that powers the gallery.

   Default image dimensions:

        Image width  :  600,
        Image height :  400,
        Thumb width  :   75,
        Thumb height :   75,

Examples:

   Default:

```bash
        ## makes gallery with default sizes
        $ python3 mkGallery.py
```      
    Arguments:
```bash
        ## makes user defined sized gallery and all web files
        $ python3 mkGallery.py image_size=700x400 thumb_size=85x85 -all
```    

###    Resizing the Gallery Images or Thumbnails:

You can do this easily for the images themselves. The thumbnails 
themselves. Or both.

1. Resizing images. Effects the entire gallery and changes the 
   image size in the json file for further use.

```bash
$ python3 mkGallery.py image_size=700x400

```

2. Resizing the thumbnails. Like above effects are the same just 
   dealing with smaller images. ^LOOK UP^

```bash
$ python3 mkGallery.py thumb_size=100x100

```

3. Change the gallery's image size and thumbnail size. Effects
   are third verse same as the first! I'm \`Enry the eighth I am.

```bash
$ python3 mkGallery.py image_size=600x350 thumb_size=100x100

```

###    Adding Images to Gallery's:

Since we already set our sizes and we are just making new gallery's or 
adding image's to them you can ignore any of the size arguments 
altogether and just run simply add files to gallery folders and run. 

```bash
$ python3 mkGallery.py

```

** Note: This will update the json file if/when images are found.
        

###    Rebuilding the JSON file:

If you ever feel that the json file needs to be manually rebuilt you can
do at any time by.

```bash
$ python3 mkGallery.py -json

```

###    Renaming Gallery's:

You can go ahead just rename the directory folders and rebuild the json
file as you need. Might build a quick helper function later.

```bash
$ python3 mkGallery.py -json

```

###    Renaming Images:

Image renaming helper will run through a set of help options to choose 
the right gallery and image to rename.

```bash
$ python3 mkGallery.py -rename-image 

```

If you want to rename images manually you can delete the image from the 
main gallery images and it's respective thumbnail in the thumbs 
directory. Then rename and move the image from the Original_Archive
directory to the gallery's main image folder and simply run:

```bash
$ python3 mkGallery.py -clean-loose-files

```

###    Removing Images:

Image removal helper will run through a set of help options to choose
the right gallery and image to remove. Once chosen all files associated
with this image will be removed.

```bash
$ python3 mkGallery.py -remove-image

```

Of course you can remove all the files manually then clean run a clean
which will remove loose files you might have missed. This will also 
rebuild the json file.

```bash
$ python3 mkGallery.py -clean-loose-files

```

###    Cleaning the gallery:

Cleaning the gallery and removing loose files will look for all the 
gallery's. Once found a master list of images is created and compared 
with the files inside the archive, thumb and text folders removing files 
that are not in the master gallery list.

```bash
$ python3 mkGallery.py -clean-loose-files

```

###    Setting the gallery's placeholder image:

Gallery image placeholder helper function that will run through a set of
helper options to find the right gallery and image to use.

```bash
$ python3 mkGallery.py -set-start-image

```

You can also do this manually by finding the file and all it's files in
its archive, thumb and text directories. Then you can rename them to
have a preceding and trailing underscore.

    Example:

        cool_pic.png | should be named | _cool_pic_.png
    

###    Gallery's and Images About Info:

Both Gallery's and Images can have about text -> .txt files. In each
gallery there can be a directory called Images_Info_Text that will hold
this gallery's about text file and all the image's text riles.

They will keep their respective gallery or image name but with a .txt


###    Saving Edits Made to the JSON File:

So I sometimes find it easier to edit the json file directly. So I made 
a shortcut to save certain aspects of the json file file to hard copy
in some way. Let me explain:

Gallery and image's about text can be written to files. 
This is really important as any time you run the mkGallery.py file
it will search and build a gallery files location object to be 
compared with the json file. When changes are found the json file 
will be remade in the file objects image and all changes to the 
json file that are unsaved WILL BE LOST!

To save json info to disk:


```bash
$ python3 mkGallery.py -json-save-info

```

###    Principal theory behind this project.

I wanted a single file a web developer could use to make some image 
gallery's and maintain them easily. This principal I extended to the
javascript file as well. 

There are no image files except for the gallery's images themselves.
All buttons are dynamically created and are size dependent to what 
you set the thumbnail images size to.

Web development is messy. Images can get out of hand. I've tried to
think of ways to make this as flexible as possible. Gallery buttons
are one less thing you have to think about while having the ability
to have your color scheme represented.

As this is one file the majority of this readme is at the head of 
the mkGallery.py file for your reference. At first glance this file
seems overwhelming at 1600+ lines of code. Just remember that as it
stands right now roughly only 1/3 of the file is actual code as seen 
below in cloc.

```bash
$ cloc mkGallery.py 
         1 text file.
         1 unique file.                              
         0 files ignored.

github.com/AlDanial/cloc v 1.74  T=0.02 s (58.6 files/s, 95847.0 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           1            452            600            584
-------------------------------------------------------------------------------
```

The javascript file is far less commented out. 


####    THINGS NOT FINISHED YET: 

1. Things are working pretty good. There will be minor tweaks. I'd
   like to figure out something to show/hide the thumbnail scrub bar.
   Something to make it obvious but unobtrusive! Still thinking on it.

2. The javascript is in desprate need of mobile fullscreen love. The
   thing is, is that I, I hate mobile; mobile development sucks. BAD!

3. I'd like to expand the php file to use the other major image 
   processing library as an option. Hell I'd also like to hard code a
   super fall-back option sing vanilla php.

       *  I also need to make is to there is a way to disable 
          fullscreen in settings.


*** NOTES: I have noticed that python3.6 and previous versions of PIL will
give really bad quality images. Very pixelated. Use python3.7+ versions of
python Pillow (PIL) for best encode.

All the files in this repository's `scripts` folder can be ahead of 
the version in the mkGallery.py file might have. The `scripts` file could 
also be broken in there. It will likely be whatever is in development. 
Sorry about that. 