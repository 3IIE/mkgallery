#!/usr/bin/env python3
"""This file will build and maintain the parts and peaces one needs for a
functional html image gallery. As of yet it only handles the basics. Here's a 
list.

    1. Backs up original files to a folder created called Original_Archive

        This file will use this original when resized and as such is the maximum
        image resolution. The fullscreen will use these images to dynamically 
        load the larges image possible through server side php.

    2. Creates the necessary images and thumbnails in their respective 
       directories.

    3. Creates and maintains the gallery's json object:

           The json object will hold the maximum hight and width for the gallery
           images and thumbnails. This means that once you have set your 
           gallery's image and thumb size you only have to pass the size
           parameters when you wish to change the size of all images in your 
           gallery. 

           The json file will also hold all your gallery's and images.

           Adding new images is as easy as pasting your image in the correct
           gallery you want and naming the file what you want the filename to
           be displayed as in the about image section and running:

               $ python3 mkGallery.py

    4. Creates all the web files. HTML, javascript, CSS and PHP in a scripts
       directory.

    5. Gives the user basic helper functions to keep their gallery clean and
       current.

Basic Usage:

    Building Gallery's:

        1. Create and name your gallery's image directory. Or many if you want.

        2. Move images into gallery's and name the images. This will support
           naming with spaces and with '_' as a word deliminator instead of 
           spaces.

        3. Move the mkGallery.py file to the same directory your gallery folders
           are in. It is important here to note that mkGallery.py will search
           literally every folder that is in the same directory as it is in. It
           will make backups, images, thumbnails and links in json to EVERY 
           image file it finds. It will do this recursively. 

           As such I advise you designate mkGallery.py and its gallery's it's
           own folder. If you must keep other images with your gallery's you 
           can keep them in hidden .directories. (see hidden files in *nix for
           further details)

        4. Once you have your images in their gallery setup the way you want you
           can now build gallery and json that powers the gallery.

           Default image dimensions:

                Image width  :  600,
                Image height :  400,
                Thumb width  :   75,
                Thumb height :   75,

            Examples:

              Default:
                ## makes gallery with default sizes

                $ python3 mkGallery.py 
            
              Arguments:
                ## makes user defined sized gallery and all web files

                $ python3 mkGallery.py image_size=700x400 thumb_size=85x85 -all
    

    Resizing the Gallery Images or Thumbnails:

        You can do this easily for the images themselves. The thumbnails 
        themselves. Or both.

            1. Resizing images. Effects the entire gallery and changes the 
               image size in the json file for further use. 

                   $ python3 mkGallery.py image_size=700x400

            2. Resizing the thumbnails. Like above effects are the same just 
               dealing with smaller images. ^LOOK UP^

                   $ python3 mkGallery.py thumb_size=100x100

            3. Change the gallery's image size and thumbnail size. Effects
               are third verse same as the first! I'm `Enry the eighth I am.

                   $ python3 mkGallery.py image_size=600x350 thumb_size=100x100


    Adding Images to Gallery's:

        Since we already set our sizes and we are just making new gallery's or 
        adding image's to them you can ignore any of the size arguments 
        altogether and just run simply add files to gallery folders and run. 

            $ python3 mkGallery.py

        ** Note: This will update the json file if/when images are found.
        

    Rebuilding the JSON file:

        If you ever feel that the json file needs to be manually rebuilt you can
        do at any time by.

            $ python3 mkGallery.py -json


    Renaming Gallery's:

        You can go ahead just rename the directory folders and rebuild the json
        file as you need. Might build a quick helper function later.

            $ python3 mkGallery.py -json


    Renaming Images:

        Image renaming helper will run through a set of help options to choose 
        the right gallery and image to rename.

            $ python3 mkGallery.py -rename-image 


        If you want to rename images manually you can delete the image from the 
        main gallery images and it's respective thumbnail in the thumbs 
        directory. Then rename and move the image from the Original_Archive
        directory to the gallery's main image folder and simply run:

            $ python3 mkGallery.py -clean-loose-files


    Removing Images:

        Image removal helper will run through a set of help options to choose
        the right gallery and image to remove. Once chosen all files associated
        with this image will be removed.

            $ python3 mkGallery.py -remove-image

        Of course you can remove all the files manually then clean run a clean
        which will remove loose files you might have missed. This will also 
        rebuild the json file.

            $ python3 mkGallery.py -clean-loose-files


    Cleaning the gallery:

        Cleaning the gallery and removing loose files will look for all the 
        gallery's. Once found a master list of images is created and compared 
        with the files inside the archive, thumb and text folders removing files 
        that are not in the master gallery list.

             $ python3 mkGallery.py -clean-loose-files


    Setting the gallery's placeholder image:

        Gallery image placeholder helper function that will run through a set of
        helper options to find the right gallery and image to use.

            $ python3 mkGallery.py -set-start-image

        You can also do this manually by finding the file and all it's files in
        its archive, thumb and text directories. Then you can rename them to
        have a preceding and trailing underscore.

            Example:

                cool_pic.png | should be named | _cool_ping_.png
    

    Gallery's and Images About Info:

        Both Gallery's and Images can have about text -> .txt files. In each
        gallery there can be a directory called Images_Info_Text that will hold
        this gallery's about text file and all the image's text riles.

        They will keep their respective gallery or image name but with a .txt


    Saving Edits Made to the JSON File:

        So I sometimes find it easier to edit the json file directly. So I made 
        a shortcut to save certain aspects of the json file file to hard copy
        in some way. Let me explain:

            Gallery and image's about text can be written to files. 
            This is really important as any time you run the mkGallery.py file
            it will search and build a gallery files location object to be 
            compared with the json file. When changes are found the json file 
            will be remade in the file objects image and all changes to the 
            json file that are unsaved WILL BE LOST!

        To save json info to disk:

            $ python3 mkGallery.py -json-save-info


    THINGS NOT FINISHED YET: 

        1. Things are working pretty good. There will be minor tweaks. I'd
           like to figure out something to show/hide the thumbnail scrub bar.
           Something to make it obvious but unobtrusive! Still thinking on it.

        2. I'd like to expand the php file to use the other major image 
           processing library as an option. Hell I'd also like to hard code a
           super fall-back option sing vanilla php.


    *** NOTE: I have noticed that python3.6 and previous versions of PIL will
    give really bad quality images. Very pixelated. Use python3.7+ versions of
    python Pillow (PIL) for best encode.

 """
from sys import argv, exit as sExit
from os import walk, listdir, mkdir, remove
from os.path import (abspath, splitext, relpath, dirname, exists, isfile,
                     join as oJoin, split as oSplit)
from shutil import move
import json

from pathlib2 import PureWindowsPath
from PIL import Image

__version__ = 0.95


class MakeWeb():
    """
This class will build any or all of the eeb files our image gallery needs to
run.

    You can create all the web files by passing the -all flag or build 
    individually by passing one or more of your desired file creation flags.

    1.       HTML: -html 
    2. javascript: -js
    3.        php: -php
    4.        css: -css

        Examples:
            # make all files
            $ python3 mkGallery.py -all

            # make html and javascript
            $ python3 mkGallery.py -html -js

    ** Note: This class does not maintain or make the json file. The json file 
             is updated every time the file is run. See the class named
             mkGallery. This class makes all the web files."""


    def __init__(self, *args, **kargs):
        """Build our basic attributes and any or *all* files the user needs."""

        self.base_dir = abspath(dirname(__file__))

        self.scripts_dir = oJoin(self.base_dir, 'scripts')

        if '-php' in args:
            self.make_php()

        if '-html' in args:
            self.make_html()

        if '-js' in args:
            self.make_js()

        if '-css' in args:
            self.make_css()

        if '-all' in args:
            ## Here we need to make all our web files.
            self.make_html()
            self.make_php()
            self.make_js()
            self.make_css()


    def make_php(self):
        """ This method will will write our php file to our scripts directory."""

        php = """<?php
// apt-get install php-imagick
// php -m | grep imagick

// This file is designed to return image data to your AJAX call. It will attempt to resize the image to
// the largest size the original image can allow. I can't make your image larger than the original.

// File expects a URL encoded variable string with the following (keys: values):

//   width: integer - maximum desired width
//  height: integer - maximum desired height
// gallery: string <= 255 characters - gallery's folder name
//  picLoc: string <= 255 characters - picture file name

// This file will return:

// 1. width of new image
// 2. height of new image
// 3. base64 encoded image data

// the above will be returned in a string with a deliminator of _&_

// $_POST['functToGet'] = 'imageGet';
if(@$_POST['functToGet'] == 'imageGet'){

    // ** sanitize the variables. Terminate script if requirements unmet or inadequate. ** \\

    if(@$_POST['width'] !== '' && is_int((int)@$_POST['width'])){
        // user passed the width and it is an integer
        $wantWidth = (int)$_POST['width']; // set width wanted integer
    }else{
        die(); // exit without comment
    }

    if(@$_POST['height'] !== '' && is_int((int)@$_POST['height'])){
        // user passed the height and it is an integer
        $wantHeight = (int)$_POST['height']; // set height wanted integer
    }else{
        die(); // exit without comment
    }

    if(is_string(@$_POST['gallery']) && @$_POST['gallery'] !== '' && strlen(@$_POST['gallery']) < 256){
        // user has passed the gallery filename. Is String and is less than 256 characters long.
        $galleryName = $_POST['gallery']; // set our gallery name
    }else{
        die(); // exit without comment
    }

    if(is_string(@$_POST['picLoc']) && @$_POST['picLoc'] !== ''){
        
        $imgLoc = $_POST['picLoc']; // set our picture name

        if(strpos($imgLoc, '/')){
            // We really only need the filename not a file path and I think this is a file path
            $imgLoc = explode('/', $imgLoc)[1]; // set our picture name to the end of the file path
            if(strlen($imgLoc) > 255){
                // String is greater than 256 characters long.
                die();
            }

        }elseif(strlen(@$_POST['picLoc']) > 255){
            // String is greater than 256 characters long.
            die();
        }

    }else{
        die(); // exit without comment
    }
    // _________________________________________________________

    // ** Set up our original image file location ** \\

    // We need to find our current working directory's absolute path. 
    $loc = explode('/', __FILE__);

    // we need to basically step out to our parent directory. ie: ../
    $loc = array_slice($loc, 0, count($loc) - 2);

    // Our largest images are in the original archive folder separated by gallery name
    foreach([$galleryName, 'Original_Archive', $imgLoc] as $val){
        // We are building our image file path sequentially
        array_push($loc, $val);
    }

    // Join our images absolute file path into a posx path string
    $loc = implode($loc, '/');
    // _________________________________________________________

    // ** Resize our image ** \\

    // find our maximum image dimensions based on the what the user asked for previously above. 
    // We also need our image mime type.
    list($width, $height, $mime) = imgRX($loc, $wantWidth, $wantHeight);

    $img = new Imagick($loc); // create new imagick image based on our Original_Archive image
    $img->resizeImage($width, $height, imagick::COMPRESSION_NO, 1); // resize our image 
    $imgBlob = base64_encode($img->getimageblob()); // encode our image blob base64 for data
    $img->clear(); // clear our image data.
    $img->destroy(); // destroy it

    // _________________________________________________________

    // ** Return our width, height and base64 data string separated by a hopefully unique enough deliminator.
    echo $width.'_&_'.$height.'_&_'.'data:'.$mime.';base64,'.$imgBlob;
}
else{
    // the user has failed our basic (what) question
    die(); // exit without comment
}

// Here is the image resize function
function imgRX($imgPath, $maxWidth, $maxHeight) {
    // Get width and height of original image
    $size = getimagesize($imgPath);
    if($size === FALSE) return FALSE;

    $resizedWidth = $size[0];
    $resizedHeight = $size[1];

    if($resizedWidth > $maxWidth){
        $aspectRatio = $maxWidth / $resizedWidth;
        $resizedWidth = round($aspectRatio * $resizedWidth);
        $resizedHeight = round($aspectRatio * $resizedHeight);
    }
    if($resizedHeight > $maxHeight){
        $aspectRatio = $maxHeight / $resizedHeight;
        $resizedWidth = round($aspectRatio * $resizedWidth);
        $resizedHeight = round($aspectRatio * $resizedHeight);
    }

    return array($resizedWidth, $resizedHeight, $size['mime']);
}

?>
"""
        with open(oJoin(self.scripts_dir, 'gallery.php'), 'w') as file_:
            file_.write(php)


    def make_html(self):
        """
This method will get our gallery.json files ['settings'] and use them to build
our mkGallery() function calls settings object. 

This method will write our gallery.html file to our base directory.  """

        setrs = {}

        with open('scripts/gallery.json') as file_:
            setrs = json.loads(file_.read())['settings']

        gOrder = setrs['galleryOrder']
        gOrder = f"'{gOrder}'" if gOrder is not False else 'false'

        gStart = setrs['startGallery']
        gStart = f"'{gStart}'" if gStart is not False else 'false'

        html = f"""<html>
    <head>
        <title>This is a thing</title>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=0.8'>
        <!-- <script src='gsap.min.js' type='text/javascript' charset='utf-8'></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js'></script>
        <script type='text/javascript' src='scripts/gallery.min.js'></script>
        <!-- <link type="text/css" rel="stylesheet" href="scripts/gallery.css" ></link> -->
        <script>
            window.addEventListener('load', function(){{
                var gallery = document.getElementById('gallery');

                mkGallery(gallery,
                          'scripts/gallery.json', 
                          {{'topDir': '{str(setrs['topDir'])}',
                           'galleryOrder': {gOrder},
                           'startGallery': {gStart},
                           'stageMultiplier': {setrs['stageMultiplier']},
                           'btnColUp': '{setrs['btnColUp']}',
                           'btnColOver': '{setrs['btnColOver']}',
                           'btnBgTop': '{setrs['btnBgTop']}',
                           'btnBgTopAlpha': {setrs['btnBgTopAlpha']},
                           'btnBgBottom': '{setrs['btnBgBottom']}',
                           'btnBgBottomAlpha': {setrs['btnBgBottomAlpha']},
                           'nowPlayColor': '{setrs['nowPlayColor']}',
                           'folder': {{'folderBG': '{setrs['folder']['folderBG']}',
                                      'folderTab': '{setrs['folder']['folderTab']}',
                                      'tabShadow': '{setrs['folder']['tabShadow']}',
                                      'tabAccent': '{setrs['folder']['tabAccent']}'}}
                          }}
                );
            }});

        </script>
    </head>
    <body>
        <div id='gallery' style='position:absolute; left:120px; top:20px;'></div> 
    </body>
</html>"""

        with open(oJoin(self.base_dir, 'gallery.html'), 'w') as file_:
            file_.write(html)


    def make_css(self):
        """ 
This class method will build our gallery.css file. As of now it's very simple
and overrides our javascript file. In order for this file to work at all it
* must * be loaded after our gallery.js javascript file in our gallery.html
file. I really hope that makes sense? 
"""

        css = """/* 
    _____________________
    | The text elements | 
*/

/* The <p> tags used to describe */
.gallery-paragraph{
    font-size: 13px !important;
    color: #fff !important;
}

/* The <h3> tags used to title the about elements */
.gallery-title{
    font-size: 16px !important;
    color: #fff !important;
}

/*
    ____________________________________
    | The galllery background elements |
*/

/* The galllery changer background */
#gallery-bgChanger:{
    background-color: #000 !important;
    opacity: 0.5 !important;
}

/* The background while in fullscreen */
:fullscreen {
  background-color: #000;
}
"""

        with open(oJoin(self.scripts_dir, 'gallery.css'), 'w') as file_:
            file_.write(css)


    def make_js(self):
        """ 
This method is a bastard. Sorry PEP* till the end of time. But I really needed
to have only one file. One file to rule them all and in the large strings bind 
them, return them, so you can use them, in your browser.

So what does this do? It writes the minified version of our gallery's javascript
to our scripts directory.

# bash command to minify our javascript
$ minify --mangle --simplify --evaluate \
  --deadcode scripts/gallery.js > scripts/gallery.min.js

"""
        javascript = ''''use strict';if(!window.gsap){let e=document.createElement("script");e.type="text/javascript",e.src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js",document.getElementsByTagName("head")[0].appendChild(e)}function print(e){console.log(e)}function mkGallery(e,t,l,i){var s=Math.abs,n=Math.round,d=Math.ceil,r=Math.floor;function o(e){return e=e.replace("px","").replace("%",""),+e}function a(e){i&&console.log(e)}function p(){return""===l.topDir?""===J.settings.topDir?"":J.settings.topDir:l.topDir}function y(e){return""===X?e:X+"/"+e}function h(e){return e.replace(/_/g," ")}function c(){let e=document.getElementById("thumb_"+ee).parentNode,t=.5*o(document.getElementById("thumbScroller").style.width),l=document.getElementById("thumbScroller");l.scrollTo(o(e.style.left)-t+.5*o(e.children[0].style.width),0)}function g(t){let e,i=t.target||t.srcElement||window.event;e=i.id.replace("Top","_btn"),e=document.getElementById(e),gsap.to(e,{duration:.5,fill:l.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0})}function b(t){let e,i=t.target||t.srcElement||window.event;e=i.id.replace("Top","_btn"),e=document.getElementById(e),gsap.to(e,{duration:.5,fill:l.btnColUp,scale:1,x:0,y:0,overwrite:!0})}function m(e){e=e.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i,function(e,t,l,i){return t+t+l+l+i+i});var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?{r:parseInt(t[1],16),g:parseInt(t[2],16),b:parseInt(t[3],16)}:null}function u(e,t){(null==t||2>t)&&(t=111);let l,i,s,n,d;return l=m(e.replace("#","")),i=l.r,s=l.g,n=l.b,d=(299*i+587*s+114*n)/1e3,!(d>=t)}function x(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("Top",""),i=document.getElementById("stage");"gallery-minus"==l?(pe="left",f(i)):(pe="right",f(i))}function f(e){let t,l=o(e.style.width),i=o(e.style.height),a=e.children[0],p=a.children[0],y=Q.images.length;t=se?[r(.5*(l-oe)),d(.5*(l-oe)),r(.5*(l-Q.images[ee].width)),d(.5*(l-Q.images[ee].width)),0]:[r(.5*(l-Q.images[ee].width)),d(.5*(l-Q.images[ee].width)),0];document.getElementById("thumbScroller");"right"==pe&&-1!=t.indexOf(n(o(a.style.left)))&&(++ee,0>ee?ee=y-1:ee>=y&&(ee=0),gsap.to(a,{left:-s(o(a.style.width)+10)+"px",duration:1,ease:Power2.easeOut,onComplete:function(){a.style.visibility="hidden",a.style.left=l+2+"px",p.style.visibility="hidden",P(),A()}})),"left"==pe&&-1!=t.indexOf(n(o(a.style.left)))&&(--ee,0>ee?ee=y-1:ee>=y&&(ee=0),gsap.to(a,{left:l+10+"px",duration:1,ease:Power2.easeOut,onComplete:function(){a.style.visibility="hidden",a.style.left=-s(o(a.style.width)+10)+"px",p.style.visibility="hidden",P(),A()}})),c()}function v(){let t=document.getElementById("gallery-fwdBtn"),l=document.getElementById("gallery-infoBtn"),i=document.getElementById("thumbScroller"),n=document.getElementById("scrollFinder");i===void 0||null===i?(i=document.createElement("div"),i.id="thumbScroller",i.style.position="absolute",i.style.overflow="hidden",i.style.height=J.settings.thumb_size[1]+10+"px",i.style.zIndex=o(e.style.zIndex)+2,e.appendChild(i)):i.innerHTML="",i.style.right=o(l.style.right)+o(l.style.width)+4+"px",null!==n&&e.removeChild(n),se?(i.style.width=.32*o(e.style.width)+"px",i.style.bottom="0px"):(i.style.width=o(e.style.width)-3.75*o(t.style.width)-o(l.style.left)-4+"px",i.style.bottom=-s(J.settings.thumb_size[1])-15+"px",n=i.cloneNode(!0),n.id="scrollFinder",n.style.height=o(i.style.height)+10+"px",n.style.zIndex=o(e.style.zIndex)+1,n.style.right=o(i.style.right)-20+"px",n.style.opacity=.4,n.style.width=o(i.style.width)+40+"px",n.style.bottom=o(i.style.bottom)-10+"px",e.appendChild(n),n.addEventListener("mouseover",function(){clearTimeout(ye)},!1),n.addEventListener("mouseout",function(){clearTimeout(ye),ye=setTimeout(c,3000)},!1));let d=6;for(let t in Q.images){let l=Q.images[t],s=document.createElement("div");s.style.position="absolute",s.style.bottom=.5*(J.settings.thumb_size[1]-l.thumb.height)+"px",s.style.width=l.thumb.width+"px",s.style.height=l.thumb.height+"px",s.style.zIndex=o(e.style.zIndex)+3,"0"==t?s.style.left=d+"px":(d=d+Q.images[t-1].thumb.width+3,s.style.left=d+"px"),i.appendChild(s);let n=document.createElement("img");n.id="thumb_"+t,n.style.width=l.thumb.width+"px",n.style.height=l.thumb.height+"px",n.style.bottom="0px",n.style.left="0px",n.src=y(l.thumb.path),s.appendChild(n),n.addEventListener("load",function(){n.style.visibility="visible",gsap.from(n,{scaleX:0,scaleY:0,opacity:0,ease:Power2.easeOut,duration:.5})}),s.addEventListener("mouseover",E,!1)}let r=document.getElementById("galleryScrubber");(null===r||r===void 0)&&(r=document.createElement("div"),r.id="galleryScrubber",r.style.position="absolute",r.style.cursor="pointer",r.style.height=o(t.style.height)+"px"),se?(r.style.width=.33*o(e.style.width)+3*o(j)+"px",r.style.bottom=J.settings.thumb_size[1]+"px"):(r.style.width=e.style.width,r.style.bottom="0px"),r.style.right=t.style.right,r.style.zIndex=o(e.style.zIndex)+2,e.appendChild(r),r.addEventListener("mousemove",C,!1),r.addEventListener("mouseout",function(){clearTimeout(ye),ye=setTimeout(c,3200)},!1),r.addEventListener("mouseover",function(){clearTimeout(ye)},!1)}function w(t){let e,l=t.target||t.srcElement||window.event,i=document.getElementById("stage"),s=o(i.style.width),a=o(i.style.height),p=i.children[0],y=p.children[0];e=se?[r(.5*(s-oe)),d(.5*(s-oe)),r(.5*(s-Q.images[ee].width)),d(.5*(s-Q.images[ee].width)),0]:[r(.5*(s-Q.images[ee].width)),d(.5*(s-Q.images[ee].width)),0],ee=o(l.id.replace("thumb_","")),-1!=e.indexOf(n(o(p.style.left)))&&gsap.to(p,{scaleX:"0",scaleY:"0",duration:1,ease:Power2.easeOut,onComplete:function(){pe="zoom",P(),A()}})}function E(t){clearTimeout(ye);let e=t.target||t.srcElement||window.event;e.removeEventListener("mouseover",E,!1),e.addEventListener("click",w,!1),e.style.cursor="pointer"}function C(t){let e=t.target||t.srcElement||window.event,l=document.getElementById("thumbScroller"),i=o(e.style.width),s=l.scrollWidth,n=o(l.style.width),d=t.offsetX*((s-n)/(i+(i-n)))*(i/n);l.scrollTo(d,0)}function A(){let e=document.getElementById("gallery-image-info");e.innerHTML="",e.style.display="inline-block";let t=document.createElement("div");t.style.position="absolute",t.style.position="relative",t.style.margin="0px 3px 1px 0px",t.style.width=.96*o(e.style.width)+"px",t.style.height=.15*o(e.style.height)+"px",t.style.top=.02*o(e.style.height)+"px",t.style.left=.02*o(e.style.width)+"px",e.appendChild(t);let i=document.createElement("h3");i.className="gallery-title",i.style.color=l.folder.folderTab,i.style.textShadow=u(l.folder.folderTab,111)?"0px 0px 20px #ffffff":"0px 0px 20px #000000",i.style.textAlign="center",i.style.padding="0px 3px 3px 3px",i.innerHTML=h(Q.images[ee].name),t.appendChild(i);let s=document.createElement("div");s.style.position="relative",s.style.margin="0px 3px 1px 0px",s.style.overflow="auto",s.style.left=t.style.left,s.style.width=t.style.width,s.style.height=.8*o(e.style.height)+"px",s.style.bottom=.02*o(e.style.height)+"px",e.appendChild(s);let n=document.getElementById("thumb_"+ee).cloneNode(!0);n.id="infoPic",n.style.position="relative",n.style.visibility="visible",n.style.left="0px",n.style.top="0px",n.style.float="left",n.style.margin="10px 10px 10px 10px",s.appendChild(n);let d=Q.images[ee].info_text.split("\n");for(let e in d){let t=document.createElement("p");t.className="gallery-paragraph",t.style.position="relative",t.style.width=.93*o(s.style.width)+"px",t.innerHTML=d[e],t.style.textIndent="20px",t.style.color="#FFFFFF",t.style.display="block",t.style.padding="2px 2px 2px 2px",s.appendChild(t)}}function B(e){let t=te.cloneNode(!0),l=document.createElement("div");l.style.position="absolute",l.style.top="0px",l.style.left="0px",l.style.width=1.25*J.settings.thumb_size[0]+"px",l.style.height=1.25*J.settings.thumb_size[1]+"px",l.appendChild(t);let i=document.createElement("img");i.style.position="absolute",l.appendChild(i);let s,n,d;void 0===e.galleryIndex||"number"!=typeof e.galleryIndex?(s=e.images[0].thumb.height,n=e.images[0].thumb.width,d=e.images[0].thumb.path):(s=e.images[e.galleryIndex].thumb.height,n=e.images[e.galleryIndex].thumb.width,d=e.images[e.galleryIndex].thumb.path),i.src=y(d),i.addEventListener("load",function(){n>s?(i.style.top=.6*(o(l.style.height)-s)+"px",i.style.left=.3*(o(l.style.width)-n)+"px",i.style.transform="rotate(-6deg)"):(i.style.top=.7*(o(l.style.height)-s)+"px",i.style.left=.39*(o(l.style.width)-n)+"px",i.style.transform="rotate(87deg)"),i.style.visibility="visible",gsap.from(i,{scaleX:0,scaleY:0,duration:.33,ease:Power2.easeIn})});let r=l.cloneNode(!0);return r.innerHTML="",l.appendChild(r),l}function S(e,t){let i=document.getElementById("gallery-info"),s=o(i.style.width),n=o(i.style.height);gsap.to(i,{width:"0px",height:"0px",scaleX:0,scaleY:0,duration:.2,overwrite:!0,ease:Power2.easeIn}),i.innerHTML="";let d=document.createElement("div");d.style.position="absolute",d.style.overflow="auto",d.style.display="inline-block",d.style.width=.98*o(i.style.width)+"px",d.style.left=.5*(o(i.style.width)-o(d.style.width))+"px",d.style.height=i.style.height,i.appendChild(d);let r=B(e,t);r.id=t+"info",r.style.position="relative",r.style.overflow="hidden",r.style.float="left",r.style.left="0px",r.style.top="0px",r.style.margin="0px 3px 1px 0px",d.appendChild(r);let a=document.createElement("h3");a.className="gallery-title",a.style.position="relative",a.style.display="inling-block",a.style.padding="0px 0px 1px 0px",a.style.textAlign="center",a.innerHTML=h(t),a.style.color=l.folder.folderTab,a.style.textShadow=u(l.folder.folderTab,111)?"0px 0px 20px #ffffff":"0px 0px 20px #000000",d.appendChild(a);let p=e.info.split("\n");for(let l in p){let e=document.createElement("p");e.className="gallery-paragraph",e.style.display="block",e.innerHTML=p[l],e.style.padding="1px 2px 2px 2px",e.style.textIndent="20px",e.style.color="#FFFFFF",d.appendChild(e)}gsap.to(i,{scaleX:1,scaleY:1,width:s+"px",height:n+"px",duration:.2,overwrite:!0,ease:Power2.easeIn})}function I(t){let e=t.target||t.srcElement||window.event,l=e.parentNode.id,i=document.getElementById(l);i.removeEventListener("mouseover",I,!1),i.addEventListener("click",T,!1),i.addEventListener("mouseout",z,!1),e.parentNode.id!==$&&S(J.gallerys[l],l)}function z(t){let e=t.target||t.srcElement||window.event,l=document.getElementById(e.parentNode.id);l.addEventListener("mouseover",I,!1),l.removeEventListener("click",T,!1),l.removeEventListener("mouseout",z,!1),e.parentNode.id!==$&&S(Q,$)}function T(t){let e=t.target||t.srcElement||window.event,l=e.parentNode.id;Q=J.gallerys[l],$=l,S(Q,$),v(Q),ee=void 0===Q.galleryIndex?0:Q.galleryIndex;let i=document.getElementById("stage"),s=o(i.style.width),n=o(i.style.height),d=i.children[0],r=d.children[0];gsap.to(d,{scaleX:"0",scaleY:"0",duration:1,ease:Power2.easeOut,onComplete:function(){pe="zoom",P(),A()}})}function N(){let t,l,i,s,d=document.getElementById("gallery-backsplash");d!==void 0&&null!==d&&e.removeChild(d),se?(t=.5*o(e.style.width),l=.5*o(e.style.height),i=J.settings.thumb_size[1]+"px",s=.5*(o(e.style.width)-t)+"px"):(t=o(e.style.width),l=o(e.style.height),i="0px",s="0px"),d=document.createElement("div"),d.style.position="absolute",d.id="gallery-backsplash",d.style.overflow="hidden",d.style.zIndex=o(e.style.zIndex)+5,d.style.bottom=i,d.style.left=s,d.style.width=t+"px",d.style.height=l+"px",e.appendChild(d);let a=d.cloneNode(!0);a.id="gallery-bgChanger",a.style.backgroundColor="#000000",a.style.opacity=.6,a.style.top="0px",a.style.left="0px",d.appendChild(a),d.style.height="0px";let p=document.createElement("div");p.id="gallery-image-info",p.style.position="absolute",p.style.display="inline-block",p.style.zIndex=o(d.style.zIndex)+3,p.style.top="0px",p.style.left="0px",p.style.width=.48*t+"px",p.style.height=.96*l+"px",p.style.opacity=.85,d.appendChild(p),A();let y=p.cloneNode(!0);y.id="gallery-info",y.style.position="absolute",y.style.top="5px",y.style.left=.5*t+.1*(.5*t-.8*(.48*t))+"px",y.style.width=o(y.style.width)+5+"px",d.appendChild(y);let h=document.createElement("div");h.id="gallery-picker",h.style.position="absolute",h.style.overflow="auto",h.style.zIndex=o(d.style.zIndex)+2,h.style.bottom="0px",h.style.left=o(y.style.left)-(1.07*o(y.style.width)-o(y.style.width))+"px",h.style.width=1.06*o(y.style.width)+"px",h.style.opacity=0,d.appendChild(h);let c=document.createElement("div");c.style.position="absolute",h.appendChild(c);let g=0,b=0,m=!0,u=0,x=-1,f=0;for(let e in J.gallerys){x+=1;let t=B(J.gallerys[e],e);t.id=e,t.style.position="absolute",0==x%f?(b+=o(t.style.height),g=0):(g+=o(t.style.width),u<g&&(u=g)),m&&(m=!1,g=0,b=0,h.style.height=1.02*o(t.style.height)+"px",y.style.height=l-o(h.style.height)-20+"px",f=r(o(h.style.width)/(o(t.style.width)+3)),c.style.width=n(o(t.style.width)*f)+"px"),t.style.left=g+"px",t.style.top=b+"px",c.appendChild(t),t.addEventListener("mouseover",I,!1)}c.style.left=.2*(o(h.style.width)-o(c.style.width))+"px",c.style.height=c.scrollHeight+"px",S(Q,$)}function F(){ie=!0,gsap.to(le,{duration:1.1,scaleX:1,scaleY:1,x:0,y:0,overwrite:!0});let t,l=document.getElementById("gallery-backsplash"),i=document.getElementById("gallery-blank"),s=document.getElementById("gallery-picker");t=se?.5*o(e.style.height):o(e.style.height),gsap.to([l,i],{duration:1,height:t+"px",overwrite:!0}),gsap.to(s,{duration:1.3,delay:.5,opacity:1,overwrite:!0})}function L(){ie=!1,gsap.to(le,{duration:.5,scaleX:0,scaleY:0,x:.1*o(V),y:.1*o(j),overwrite:!0});let e=document.getElementById("gallery-backsplash"),t=document.getElementById("gallery-picker");gsap.to(e,{duration:1,height:"0px",overwrite:!0}),gsap.to(t,{duration:.3,opacity:0,overwrite:!0})}function k(){q=document.createElementNS(he,"svg"),q.setAttribute("baseProfile","tiny"),q.setAttribute("xlmns",he),q.setAttribute("viewBox","0 0 20 15"),q.setAttribute("preserveAspectRatio","none"),q.setAttribute("height",o(V)),q.setAttribute("width",o(j)),q.setAttribute("x","0"),q.setAttribute("y","0"),q.style.display="block";let e=document.createElementNS(he,"g");e.setAttribute("fill",l.btnBgBottom),q.appendChild(e);let t=document.createElementNS(he,"path");t.setAttribute("d","M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z"),e.appendChild(t);let i=document.createElementNS(he,"path");i.setAttribute("d","M15.716.524c2.026 0 3.676 1.226 3.676 2.73v8.5c0 1.505-1.65 2.73-3.676 2.73H4.283c-2.026 0-3.675-1.226-3.675-2.73v-8.5c0-1.505 1.648-2.73 3.675-2.73h11.433m0-.304H4.283C2.037.22.2 1.587.2 3.257v8.498c0 1.67 1.837 3.034 4.083 3.034h11.433c2.246 0 4.084-1.37 4.084-3.04v-8.5c0-1.67-1.838-3.035-4.084-3.035z"),e.appendChild(i);let s=document.createElementNS(he,"linearGradient");s.setAttribute("gradientUnits","userSpaceOnUse"),s.setAttribute("x1","9.999"),s.setAttribute("y1","4.301"),s.setAttribute("x2","9.999"),s.setAttribute("y2","11.851");let n=document.createElementNS(he,"stop");n.setAttribute("offset","0"),n.setAttribute("stop-color","#fff"),s.appendChild(n);let d=document.createElementNS(he,"stop");d.setAttribute("offset","1"),s.appendChild(d),q.appendChild(s);let r=document.createElementNS(he,"path");r.setAttribute("d","M15.716.828H4.282c-1.8 0-3.265 1.09-3.265 2.428v8.498c0 1.34 1.464 2.427 3.265 2.427h11.434c1.8 0 3.267-1.08 3.267-2.42v-8.5c0-1.34-1.466-2.426-3.267-2.426zm2.858 10.926c0 1.17-1.283 2.124-2.858 2.124H4.282c-1.575 0-2.857-.953-2.857-2.124V3.256c0-1.17 1.282-2.125 2.857-2.125h11.434c1.575 0 2.858.96 2.858 2.13v8.5z"),r.setAttribute("fill","url(#a)"),q.appendChild(r);let a=document.createElementNS(he,"path");a.setAttribute("d","M15.716.828H4.283c-1.8 0-3.267 1.09-3.267 2.428V7.65c2.72.522 5.766.815 8.984.815s6.263-.293 8.983-.814V3.26c0-1.34-1.465-2.428-3.267-2.428z"),a.setAttribute("fill",l.btnBgTop),q.appendChild(a)}function H(){K=document.createElement("div"),K.id="gallery-infoBtn",K.style.position="absolute",K.style.width=.75*o(V)+"px",K.style.height=.75*o(V)+"px",K.style.zIndex=o(e.style.zIndex)+5;let t=document.createElementNS(he,"svg");t.setAttribute("baseProfile","tiny"),t.setAttribute("xlmns",he),t.setAttribute("viewBox","0 0 20 20"),t.setAttribute("preserveAspectRatio","none"),t.setAttribute("height",.75*o(V)),t.setAttribute("width",.75*o(V)),t.setAttribute("x","0"),t.setAttribute("y","0"),le=document.createElementNS(he,"ellipse"),le.setAttribute("cx","9.7908363"),le.setAttribute("cy","7.3207169"),le.setAttribute("rx","5.9063745"),le.setAttribute("ry","5.9262953"),le.setAttribute("fill",l.btnColOver),gsap.to(le,{duration:.1,scaleX:0,scaleY:0,x:.1*o(V),y:.1*o(j),overwrite:!0});let i=document.createElementNS(he,"g");t.appendChild(i),i.appendChild(le);let s=document.createElementNS(he,"path");s.setAttribute("d","m 11.281,8.145 v 2.633 c 0.125,0.072 0.377,0.23 0.377,0.49 0,0.418 -0.598,0.678 -1.415,0.678 -0.974,0 -2.294,-0.217 -2.294,-0.707 0,-0.173 0.251,-0.346 0.408,-0.403 V 8.158 C 6.77,8.102 5.235,7.968 3.766,7.774 c 0.291,3.312 2.933,5.902 6.151,5.902 3.214,0.004 5.852,-2.578 6.15,-5.883 -1.524,0.198 -3.135,0.306 -4.786,0.352 z"),s.setAttribute("fill",l.btnBgBottom),i.appendChild(s);let n=document.createElementNS(he,"path");n.setAttribute("d","m 9.896,1.318 c -3.402,0 -6.16,2.759 -6.16,6.165 0,0.194 0.012,0.386 0.029,0.574 1.465,0.184 3.012,0.286 4.593,0.338 V 6.711 C 8.295,6.625 8.043,6.48 8.043,6.235 8.012,5.932 8.86,5.557 9.583,5.557 c 1.918,0 1.981,0.433 1.981,0.836 0,0.13 -0.094,0.289 -0.283,0.375 V 8.406 C 12.918,8.361 14.514,8.26 16.027,8.074 16.047,7.88 16.058,7.683 16.058,7.483 16.059,4.078 13.299,1.318 9.896,1.318 Z m -0.029,3.49 c -0.975,0 -1.886,-0.49 -1.886,-1.341 0,-0.418 0.503,-0.663 1.414,-0.663 1.195,0 1.886,0.577 1.886,1.312 0,0.403 -0.408,0.692 -1.414,0.692 z"),n.setAttribute("fill",l.btnBgTop),i.appendChild(n);let d=document.createElementNS(he,"g");t.appendChild(d);let r=document.createElementNS(he,"path");r.setAttribute("d","M16.021,8.484c0,0-0.041,0.142-0.117,0.405    c-0.038,0.132-0.086,0.293-0.144,0.484c-0.038,0.188-0.138,0.415-0.278,0.674c-0.135,0.253-0.271,0.547-0.45,0.833    c-0.208,0.261-0.435,0.54-0.675,0.841c-0.267,0.275-0.597,0.521-0.92,0.804c-0.352,0.246-0.762,0.444-1.168,0.679    c-0.879,0.321-1.889,0.605-2.962,0.485c-0.28-0.032-0.509-0.021-0.84-0.096c-0.248-0.072-0.499-0.146-0.752-0.223    c-0.252-0.054-0.513-0.188-0.774-0.329c-0.255-0.145-0.539-0.26-0.77-0.438C5.214,11.93,4.461,10.96,3.96,9.86    C3.504,8.73,3.403,7.48,3.579,6.271c0.263-1.162,0.817-2.318,1.638-3.189C6.071,2.249,7.101,1.634,8.203,1.36    C9.3,1.15,10.43,1.118,11.398,1.479c0.492,0.102,0.93,0.38,1.357,0.603c0.428,0.231,0.748,0.58,1.104,0.847    c0.281,0.347,0.574,0.66,0.799,0.995c0.188,0.359,0.394,0.687,0.531,1.017c0.107,0.343,0.211,0.663,0.305,0.96    c0.059,0.304,0.074,0.591,0.107,0.846c0.099,0.51-0.027,0.915-0.024,1.188c-0.022,0.274-0.036,0.42-0.036,0.42L16.021,8.484z     M15.541,8.352c0,0,0.13-1.099,0.02-1.604c-0.041-0.252-0.063-0.536-0.129-0.834c-0.102-0.291-0.209-0.604-0.326-0.94    c-0.146-0.321-0.356-0.637-0.549-0.982c-0.229-0.322-0.523-0.621-0.805-0.951c-0.675-0.552-1.429-1.104-2.417-1.337    c-0.943-0.325-2.02-0.258-3.054-0.035C7.245,1.954,6.292,2.563,5.514,3.36C4.769,4.208,4.309,5.224,4.072,6.357    C3.944,7.477,4.066,8.613,4.504,9.628c0.484,0.983,1.184,1.836,2.05,2.413c0.208,0.152,0.443,0.23,0.658,0.352    c0.213,0.116,0.427,0.229,0.7,0.281c0.255,0.067,0.508,0.135,0.758,0.202c0.173,0.031,0.462,0.029,0.682,0.047    c0.946,0.086,1.815-0.196,2.571-0.487c0.347-0.217,0.697-0.395,0.995-0.612c0.271-0.256,0.553-0.472,0.773-0.715    c0.199-0.263,0.387-0.509,0.56-0.734c0.142-0.239,0.233-0.471,0.34-0.669c0.052-0.102,0.101-0.194,0.146-0.284    c0.039-0.1,0.061-0.211,0.089-0.305c0.051-0.192,0.094-0.356,0.129-0.489c0.067-0.267,0.106-0.407,0.106-0.407L15.541,8.352z"),r.setAttribute("fill",l.btnBgBottom),d.appendChild(r),K.appendChild(t),e.appendChild(K),t.addEventListener("click",function(){ie?L():F()},!1)}function M(){te=document.createElementNS(he,"svg"),te.setAttribute("baseProfile","tiny"),te.setAttribute("xlmns",he),te.setAttribute("viewBox","0 0 40 40"),te.setAttribute("preserveAspectRatio","none"),te.setAttribute("height",1.25*J.settings.thumb_size[0]+"px"),te.setAttribute("width",1.25*J.settings.thumb_size[0]+"px"),te.setAttribute("x","0"),te.setAttribute("y","0"),te.style.display="block";let e=document.createElementNS(he,"g");e.setAttribute("fill",l.btnBgBottom),te.appendChild(e);let t=document.createElementNS(he,"path");t.setAttribute("id","folderBody"),t.setAttribute("fill","#00ADEE"),t.setAttribute("fill",l.folder.folderBG),t.setAttribute("fill-rule","evenodd"),t.setAttribute("d","M18.601,35.785c-5.4,0-10.8,0-16.2-0.001c-1.267,0-1.398-0.116-1.398-1.341\nc-0.003-10.533-0.003-21.067,0-31.601c0-1.307,0.157-1.458,1.413-1.458c4.167-0.001,8.333,0.024,12.5-0.018\nc0.99-0.01,1.305,0.407,1.29,1.305c-0.02,1.2,0.045,2.403-0.022,3.599c-0.043,0.756,0.188,0.937,0.934,0.933\nc5.9-0.032,11.8-0.019,17.7-0.018c1.252,0,1.382,0.12,1.383,1.358c0.003,8.633,0.003,17.266,0,25.9\nc-0.001,1.226-0.132,1.342-1.398,1.342C29.4,35.785,24.001,35.785,18.601,35.785z"),e.appendChild(t);let i=document.createElementNS(he,"path");i.setAttribute("fill",l.folder.tabShadow),i.setAttribute("d","M15.601,2.983c0.439,1.293,0.166,2.622,0.147,3.93\nc-0.008,0.532-0.605,0.662-1.085,0.663c-4.094,0.01-8.188,0.007-12.282,0.006c-0.654,0-0.979-0.335-0.98-0.987\nC1.4,5.597,1.371,4.597,1.41,3.6c0.033-0.847,0.53-1.396,1.372-1.402c3.894-0.031,7.789-0.013,11.683-0.014\nc0.204,0,0.4,0.02,0.537,0.199c-0.086,0.097-0.19,0.167-0.313,0.208C12.277,2.753,9.863,2.618,7.45,2.666\nc-1.553,0.03-3.107-0.077-4.655,0.066C2.436,2.858,2.187,3.109,2.007,3.436C1.781,4.534,1.785,5.629,2.012,6.722\nc0.137,0.163,0.315,0.257,0.514,0.321c4.046,0.094,8.089,0.118,12.129-0.02c0.2-0.082,0.379-0.193,0.514-0.367\nc0.318-0.759,0.096-1.554,0.156-2.329c0.028-0.357-0.025-0.721,0.079-1.074C15.444,3.145,15.51,3.055,15.601,2.983z"),e.appendChild(i);let s=document.createElementNS(he,"path");s.setAttribute("fill",l.folder.folderTab),s.setAttribute("id","tabber"),s.setAttribute("d","M14.799,2.58c0.199,0.202,0.397,0.403,0.597,0.604\nc0.169,1.267,0.167,2.533,0.001,3.799c-0.133,0.068-0.267,0.136-0.4,0.204c-0.312,0.169-0.651,0.088-0.976,0.089\nc-3.614,0.006-7.227,0.006-10.841,0c-0.325-0.001-0.664,0.081-0.976-0.09C2.037,7.179,1.885,7.144,1.797,6.981\nC1.644,5.715,1.635,4.449,1.804,3.183c0.2-0.201,0.399-0.401,0.599-0.603c0.312-0.171,0.651-0.089,0.976-0.09\nc3.481-0.007,6.963-0.007,10.444,0C14.148,2.491,14.487,2.409,14.799,2.58z"),e.appendChild(s);let n=document.createElementNS(he,"path");n.setAttribute("fill",l.folder.tabAccent),n.setAttribute("d","M2.204,7.187c4.264,0,8.529,0,12.793,0c-0.228,0.278-0.545,0.193-0.833,0.193\nc-3.709,0.005-7.418,0.005-11.127,0C2.749,7.38,2.431,7.465,2.204,7.187z"),e.appendChild(n);let d=document.createElementNS(he,"path");d.setAttribute("fill",l.folder.tabAccent),d.setAttribute("d","M14.799,2.58c-4.132,0-8.264,0-12.396,0c0.296-0.324,0.686-0.19,1.035-0.191\nc3.787-0.009,7.574-0.006,11.361-0.005C14.8,2.45,14.8,2.515,14.799,2.58z"),e.appendChild(d);let r=document.createElementNS(he,"path");r.setAttribute("fill",l.folder.tabAccent),r.setAttribute("d","M1.804,3.183C1.802,4.449,1.8,5.715,1.797,6.981C1.484,5.715,1.472,4.449,1.804,3.183z"),e.appendChild(r);let o=document.createElementNS(he,"path");o.setAttribute("fill",l.folder.tabAccent),o.setAttribute("d","M15.397,6.984c0-1.267-0.001-2.533-0.001-3.799c0.068,0,0.136,0,0.204,0\nC15.486,4.449,15.84,5.737,15.397,6.984z"),e.appendChild(o)}function O(e){let t=["fullscreenchange","mozfullscreenchange","MSFullscreenChange","webkitfullscreenchange"];if("mk"==e)for(let e in t)document.addEventListener(t[e],_,!1);else for(let e in t)document.removeEventListener(t[e],_,!1)}function _(){let e=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;(null===e||e===void 0||Object.getPrototypeOf(e)===Object.prototype)&&(G(),O("rm"))}function G(){document.querySelector("meta[name=\"viewport\"]").content=ne,Z.removeEventListener("click",G,!1),Z.addEventListener("click",D,!1),ie&&(L(),setTimeout(null,1e3)),document.exitFullscreen?document.exitFullscreen():document.webkitExitFullscreen?document.webkitExitFullscreen():document.mozCancelFullScreen?document.mozCancelFullScreen():document.msExitFullscreen?document.msExitFullscreen():a("Your browser does not support full screen.");let e=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;(e===void 0||Object.getPrototypeOf(e)===Object.prototype)&&(se=!1,pe="zoom",P(!0))}function D(){if(document.querySelector("meta[name=\"viewport\"]").content="width=device-width, height=device-height, initial-scale=1",Z.removeEventListener("click",D,!1),Z.addEventListener("click",G,!1),ie){let e=document.getElementById("gallery-backsplash");e.style.left=.5*(de-o(e.style.width))+"px",L(),setTimeout(null,1e3)}e.requestFullscreen?e.requestFullscreen():e.mozRequestFullScreen?e.mozRequestFullScreen():e.webkitRequestFullscreen?e.webkitRequestFullscreen():e.msRequestFullscreen?e.msRequestFullscreen():a("Your browser does not support full screen.");let t=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;if(void 0===t||Object.getPrototypeOf(t)!==Object.prototype){let e=stage.children[0].children[0];se=!0,e.style.visibility="hidden",pe="zoom",P(!0),O("mk")}}function P(e){let t=stage.children[0],l=t.children[0],i=o(stage.style.width),n=o(stage.style.height),d=Q.images[ee].width,r=Q.images[ee].height;if(se){let o,a,p=new XMLHttpRequest;if(o="functToGet=imageGet&width="+de+"&height="+(re-1.1*J.settings.thumb_size[1]),o+="&gallery="+escape($)+"&picLoc="+escape(Q.images[ee].path),a="scripts/gallery.php",""!==X){let e=X+"/";a=e+a}p.open("POST",a,!0),p.setRequestHeader("Content-type","application/x-www-form-urlencoded"),p.onreadystatechange=function(){if(4==p.readyState&&200==p.status){let o=p.responseText;o=o.split("_&_"),d=o[0],r=o[1],t.removeChild(l),l=document.createElement("img"),t.appendChild(l),l.src=o[2],oe=d,ae=r,l.id=$+"_"+ee,l.style.height=r+"px",l.style.width=d+"px",t.style.width=d+"px",t.style.height=r+"px",t.style.top=.5*(n-r)+"px",void 0!==e&&null!==e&&!1!==e?U():"zoom"===pe?(t.style.scaleX=0,t.style.scaleY=0,t.style.visibility="visible",gsap.to(t,{left:.5*(i-d)+"px",scaleX:1,scaleY:1,duration:1,ease:Power2.easeIn})):(t.style.visibility="visible","left"==pe&&(t.style.left=-s(d)+"px"),gsap.to(t,{left:.5*(i-d)+"px",duration:1,ease:Power2.easeIn}))}},p.send(o)}else t.removeChild(l),l=document.createElement("img"),t.appendChild(l),l.src=y(Q.images[ee].path),l.addEventListener("load",function(){l.id=$+"_"+ee,l.style.height=r+"px",l.style.width=d+"px",t.style.width=d+"px",t.style.height=r+"px",t.style.top=.5*(n-r)+"px",t.style.visibility="visible",void 0!==e&&null!==e&&!1!==e?U():"zoom"===pe?gsap.to(t,{left:.5*(i-d)+"px",scaleX:1,scaleY:1,duration:1,ease:Power2.easeIn}):("left"==pe&&(t.style.left=-s(d)+"px"),gsap.to(t,{left:.5*(i-d)+"px",duration:1,ease:Power2.easeIn}))})}function U(){let t=document.getElementById("gallery-fwdBtn"),i=document.getElementById("gallery-bckBtn"),n=document.getElementById("stage"),d=document.getElementById("gallery-imgHolder"),r=d.children[0];se?(e.style.width=de+"px",e.style.height=re-J.settings.thumb_size[1]+"px",t.style.bottom="0px",t.style.right=.3*de+o(t.style.width)+"px",i.style.bottom="0px",i.style.left=.3*de+o(t.style.width)+"px",Z.style.right=o(t.style.right)+o(t.style.width)+3+"px",Z.style.bottom="0px",K.style.right=o(Z.style.right)-3+"px",K.style.bottom=s(.6*o(V))+"px"):(e.style.width=J.settings.img_size[0]+"px",e.style.height=J.settings.img_size[1]+"px",t.style.bottom=-s(o(V))+"px",t.style.right="0px",i.style.bottom=-s(o(V))+"px",i.style.left="0px",Z.style.bottom=-s(1.25*o(V))+"px",Z.style.right=o(j)+3+"px",K.style.right=o(j)+"px",K.style.bottom=-s(.75*o(V))+"px"),n.style.width=o(e.style.width)*l.stageMultiplier+"px",n.style.height=o(e.style.height)+"px",n.style.left="0px",n.style.top="0px",d.style.top=o(n.style.top)+.5*(o(n.style.height)-o(r.style.height))+"px",d.style.left=.5*(o(n.style.width)-o(r.style.width))+"px",n.style.left=o(n.style.width)>o(e.style.width)?-s(.5*(o(n.style.width)-o(e.style.width)))+"px":"0px",v(Q),N()}function R(){let t=document.createElement("div");t.id="gallery-fwdBtn",t.style.position="absolute",t.style.width=o(j)+"px",t.style.height=o(V)+"px",t.style.zIndex=o(e.style.zIndex)+5;let i=document.createElement("div");i.style.width=j,i.style.height=V,i.style.position="absolute",i.style.visibility="visible",i.style.top="0px",i.style.left="0px",W.push(i);let s=document.createElementNS(he,"svg");s.setAttribute("xlmns",he),s.setAttribute("viewBox","0 0 20 15"),s.setAttribute("preserveAspectRatio","none"),s.setAttribute("height",o(V)),s.setAttribute("width",o(j)),s.setAttribute("x","0px"),s.setAttribute("y","0px"),s.style.position="absolute";let n=document.createElementNS(he,"path");n.setAttribute("d","M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z"),n.setAttribute("fill",l.btnColUp),n.setAttribute("id","gallery-plus_btn"),n.style.zIndex=o(e.style.zIndex)+5;let d=document.createElement("div");d.id="gallery-plusTop",d.style.height=V,d.style.width=j,d.style.position="absolute",d.style.zIndex=o(e.style.zIndex)+6,d.style.top="0px",d.style.left="0px",d.style.padding="5px",i.appendChild(d),s.appendChild(n),i.appendChild(s),i.appendChild(q),t.appendChild(i),e.appendChild(t);let r=q.cloneNode(!0),a=document.createElement("div");a.id="gallery-bckBtn",a.style.position="absolute",a.style.width=o(j)+"px",a.style.height=o(V)+"px",a.style.zIndex=o(e.style.zIndex)+6,a.style.transform="scale(-1,1)";let p=i.cloneNode(!0),h=p.getElementsByTagName("*");for(let e in h)"gallery-plusTop"===h[e].id&&(h[e].id="gallery-minusTop"),"gallery-plus_btn"===h[e].id&&(h[e].id="gallery-minus_btn");a.appendChild(p),W.push(p),e.appendChild(a),Z=document.createElement("div"),Z.id="gallery-FSbtn",Z.style.zIndex=a.style.zIndex;let c=.65;Z.style.width=o(V)*c+"px",Z.style.height=o(V)*c+"px",Z.style.position="absolute",Z.style.overflow="hidden";let g=q.cloneNode(!0);g.setAttribute("viewBox","0 0 20 20"),g.setAttribute("height",o(V)*c),g.setAttribute("width",o(V)*c);let b=document.createElementNS(he,"svg");b.setAttribute("xlmns",he),b.setAttribute("viewBox","0 0 20 20"),b.setAttribute("preserveAspectRatio","none"),b.setAttribute("height",o(V)*c),b.setAttribute("width",o(V)*c),b.setAttribute("x","0px"),b.setAttribute("y","0px"),b.style.position="absolute";let m=document.createElementNS(he,"path");m.setAttribute("d","M14.327 11.612h.63L14.95 4h-.63v.56h-.908v-.586H6.608v.004h-.02v.582h-.913v-.554h-.633l.006 7.61h.63v-.542h.915v.568h6.817v-.568h.914l.003.538zM7.54 10.576L7.538 5.04h4.923l.008 5.536H7.54zm-1.863-2.52h.915v1.03H5.68v-1.03zm.914-.48h-.91v-1.03h.915v1.03zm6.82-1.03h.916v1.03h-.913l-.002-1.03zm0 1.51h.917v1.03h-.914v-1.03zm.91-3.017l.005 1.03h-.915V5.04h.912zm-7.73 0v1.03h-.91V5.04h.913zm-.91 5.55V9.56h.916v1.03H5.68zm7.735 0v-.018l-.002-1.012h.916v1.03h-.914z"),m.setAttribute("fill",l.btnColUp),m.setAttribute("id","gallery-fScreen_btn");let u=document.createElement("div");u.id="gallery-fScreenTop",u.style.height=o(V)*c+"px",u.style.width=o(V)*c+"px",u.style.position="absolute",u.style.zIndex=o(e.style.zIndex)+6,Z.appendChild(u),b.appendChild(m),Z.appendChild(b),Z.appendChild(g),W.push(Z),e.appendChild(Z),Q=""==l.startGallery?Object.values(J.gallerys)[0]:J.gallerys[l.startGallery],$=Object.keys(J.gallerys).find(e=>J.gallerys[e]===Q);let x=Q.images,f=document.createElement("img");f.style.visibility="hidden",f.style.positiion="absolute",f.style.width=x[0].width+"px",f.style.height=x[0].height+"px",f.style.left="0px",f.style.top="0px",f.addEventListener("load",function(){f.style.visibility="visible",gsap.from(f,{scaleX:0,scaleY:0,opacity:0,ease:Power2.easeOut,duration:.5})});let v=document.createElement("div");v.id="stage",v.style.zIndex=o(e.style.zIndex)+1,v.style.position="absolute",v.style.overflow="hidden";let w=document.createElement("div");w.id="gallery-imgHolder",w.style.position="absolute",w.style.width=f.style.width,w.style.height=f.style.height,w.style.zIndex=o(e.style.zIndex)+5,w.appendChild(f),f.style.zIndex=o(w.style.zIndex)+1,ee=0,f.id=$+"_"+ee,f.src=y(x[0].path),v.appendChild(w),e.appendChild(v),U()}var Y,X,q,V,j,W,Z,J,K,Q,$,ee,te,le,ie,se,ne,de,re,oe,ae,pe,ye;W=[],ie=!1,se=!1,oe=1,ae=1;ne=document.querySelector("meta[name=\"viewport\"]").content,setTimeout(null,300),de=screen.width||document.documentElement.clientWidth||document.body.clientWidth,re=screen.height||document.documentElement.clientHeight||document.body.clientHeight,(e===void 0||null===e||""===e)&&(e=document.getElementById("gallery")),Y=void 0===t||null===t||""===t?"scripts/gallery.json":t,(i===void 0||null===i||!0!==i)&&(i=!1);let he="http://www.w3.org/2000/svg";l===void 0||null===l||Object.getPrototypeOf(l)!==Object.prototype?(console.log("No settings."),l={topDir:"",galleryOrder:!1,startGallery:!1,stageMultiplier:1,btnColUp:"#458CCB",btnColOver:"#72ABDD",btnBgTop:"#181EbF",btnBgTopAlpha:1,btnBgBottom:"#10100A",btnBgBottomAlpha:1,nowPlayColor:"#91C7F6",folder:{folderBG:"#00ADEE",folderTab:"#00BEF2",tabShadow:"#00A4E0",tabAccent:"#00A4E2"}}):(l.topDir===void 0||"string"!=typeof l.topDir?(l.topDir="",a("Setting-topDir ( * Default * )")):a("Setting-topDir (Type expected, String:URL): "+l.topDir),l.galleryOrder===void 0||"[object Array]"!==Object.prototype.toString.call(l.galleryOrder)?(l.galleryOrder=!1,a("Setting-galleryOrder ( * Default * )")):a("Setting-galleryOrder (Type expected, Array): "+l.galleryOrder),l.startGallery===void 0||"string"!=typeof l.startGallery?(l.startGallery="",a("Setting-startGallery ( * Default * )")):a("Setting-startGallery (Type expected, String:URL): "+l.startGallery),l.stageMultiplier===void 0||null===l.stageMultiplier?(l.stageMultiplier=1,a("Setting-stageMultiplier ( * Default * )")):"number"==typeof l.stageMultiplier&&0==l.stageMultiplier%1&&(l.stageMultiplier=1,a("Setting-stageMultiplier (Type expected, Integer 0.0-1.0): "+l.stageMultiplier)),l.btnColUp===void 0||"string"!=typeof l.btnColUp?(l.btnColUp="#458CCB",a("Setting-btnColUp ( * Default * )")):a("Setting-btnColUp (Type expected, Hexadecimal Color String): "+l.btnColUp),l.btnColOver===void 0||"string"!=typeof l.btnColOver?(l.btnColOver="#72ABDD",a("Setting-btnColOver ( * Default * )")):a("Setting-btnColOver (Type expected, Hexadecimal Color String): "+l.btnColOver),l.btnBgTop===void 0||"string"!=typeof l.btnBgTop?(l.btnBgTop="#181EbF",a("Setting-btnBgTop ( * Default * )")):a("Setting-btnBgTop (Type expected, Hexadecimal Color String): "+l.btnBgTop),l.btnBgTopAlpha===void 0||null===l.btnBgTopAlpha||0==l.btnBgTopAlpha%1?(l.btnBgTopAlpha=1,a("Setting-btnBgTopAlpha ( * Default * )")):a("Setting-btnBgTopAlpha (Type expected, Integer 0.0-1.0): "+l.btnBgTopAlpha),l.btnBgBottom===void 0||"string"!=typeof l.btnBgBottom?(l.btnBgBottom="#10100A",a("Setting-btnBgBottom ( * Default * )")):a("Setting-btnBgBottom (Type expected, Hexadecimal Color String): "+l.btnBgBottom),l.btnBgBottomAlpha===void 0||null===l.btnBgBottomAlpha||0==l.btnBgBottomAlpha%1?a("Setting-btnBgBottomAlpha ( * Default * )"):a("Setting-btnBgBottomAlpha (Type expected, Float 0.0-1.0): "+l.btnBgBottomAlpha),l.nowPlayColor===void 0||"string"!=typeof l.nowPlayColor?(l.nowPlayColor="#458CCB",a("Setting-nowPlayColor ( * Default * )")):a("Setting-nowPlayColor (Type expected, Hexadecimal Color String): "+l.nowPlayColor),l.folder===void 0||null===l.folder?l.folder={folderBG:l.btnBgTop,folderTab:l.btnColUp,tabShadow:l.btnBgBottom,tabAccent:l.btnOver}:Object.getPrototypeOf(l.folder)===Object.prototype?(l.folder.folderBG!==void 0||null!==l.folder.folderBG||"string"!=typeof l.folder.folderBG?l.folder.folderBG=l.btnBgTop:a("Setting-folderBG (Type expected, Hexadecimal Color String): "+l.folder.folderBG),l.folder.folderTab!==void 0||null!==l.folder.folderTab||"string"!=typeof l.folder.folderTab?l.folder.folderTab=l.btnColUp:a("Setting-folderTab (Type expected, Hexadecimal Color String): "+l.folder.folderTab),l.folder.tabShadow!==void 0||null!==l.folder.tabShadow||"string"!=typeof l.folder.tabShadow?l.folder.tabShadow=l.btnBgBottom:a("Setting-tabShadow (Type expected, Hexadecimal Color String): "+l.folder.tabShadow),l.folder.tabAccent!==void 0||null!==l.folder.tabAccent||"string"!=typeof l.folder.tabAccent?l.folder.tabAccent=l.btnBgBottom:a("Setting-tabAccent (Type expected, Hexadecimal Color String): "+l.folder.tabAccent)):"string"==typeof l.folder&&"blue"==l.folder||"Blue"==l.folder?l.folder={folderBG:"#00ADEE",folderTab:"#00BEF2",tabShadow:"#00A4E0",tabAccent:"#00A4E2"}:l.folder={folderBG:l.btnBgTop,folderTab:l.btnColUp,tabShadow:l.btnBgBottom,tabAccent:l.btnOver});let ce,ge=new XMLHttpRequest;Y.includes(".json")?ge.open("GET",Y,!0):(ce=l.playlist?"functToGet=imageSort&playlist="+l.playlist.join(","):"functToGet=mkGallery",ge.open("POST",Y,!0)),J=null,ge.setRequestHeader("Content-type","application/x-www-form-urlencoded"),ge.onreadystatechange=function(){if(4==ge.readyState&&200==ge.status){let t=ge.responseText;for(let l in J=JSON.parse(t),X=p(),e.style.width=J.settings.img_size[0],e.style.height=J.settings.img_size[1],""==e.style.zIndex&&(e.style.zIndex=o(e.parentNode.style.zIndex)+1),V=.6*J.settings.thumb_size[1]+"px",j=.3*J.settings.thumb_size[0]+"px",k(),H(),M(),R(),W){let e=W[l];e.addEventListener("mouseover",g,!1),e.addEventListener("mouseout",b,!1),"gallery-FSbtn"===e.id?Z.addEventListener("click",D,!1):e.addEventListener("click",x,!1)}N(),L()}},Y.includes(".json")?ge.send(null):ge.send(ce)}'''.replace('\n', '\\n')

        with open(oJoin(self.scripts_dir, 'gallery.min.js'), 'w') as file_:
            file_.write(javascript.replace('"viewport"', "'viewport'"))


## _______________________________________________________________##


class Clean():
    """
This class is designed to handle the major file operations that I think are
helpful to maintaining multiple gallery's like:

    1: Cleaning loose files   |  flag: -clean-loose-files
        
        For when/if your files get in the archive, thumb or text files differ
        from their top directory list if images.

    2: Removing images files  |  flag: -remove-image

        Keeping the images and text file names the same across multiple 
        directories.

    3: Renaming images files  |  flag: -rename-image

        See # 2.

    4: Setting gallery image  |  flag: -set-start-image

        Sets the image the gallery should start on. This is also the same image
        used on the folder icons.

"""
    def __init__(self, *args):
        """ 
Setup and build our gallery cleaner object. This method expects one or more 
flags outlined previously in the class doc file.

"""
        self.base_dir = abspath(dirname(__file__))

        self.json_update = False

        if '-clean-loose-files' in args:
            self.clean(True)

        if '-remove-image' in args:
            self.remove()

        if '-rename-image' in args:
            self.rename_image()

        if '-set-start-image' in args:
            self.set_start_image()


    def get_gallerys(self):
        """
This method will find the gallery's by directories this program sets up. This 
method will assume that the user has not removed any of those directory's and
returns a gallery name and location object or None.

"""
        gallery = {}
        galDirs = ['Original_Archive', 'thumbs', 'Images_Info_Text']
        galNum = 0

        for path_, dirs, _ in walk(self.base_dir):
            if [1, 1, 1] == [1 if x in dirs else 0 for x in galDirs]:
                galNum += 1
                gallery[galNum] = {'name': oSplit(path_)[1],
                                   'path': path_}

        return None if gallery == {} else gallery


    def ask_for_gallery(self, ask=''):
        """
This will ask the user which gallery they want to use and return a gallery name 
and location object.
"""
        gallerys = self.get_gallerys()

        if ask == '':
            ask = 'Which gallery would you like to use?\n'

        for galNum, gallery in gallerys.items():
            ask += f"  {galNum}: {gallery['name']}\n"

        ask += ' Enter number or name:'

        try:
            while True:
                message = input(f'{ask}\n> ')

                if message.isdigit():
                    gallery = gallerys[int(message)]
                else:
                    for gal in gallerys.values():
                        if message == gal['name']:
                            gallery = gal

                return gallery
        except (KeyboardInterrupt, SystemExit):
            sExit('\nGoodbye!')


    def remove(self, file_=None):
        """
This method will remove an image file from a gallery and clean up any straggling
sibling files in the other gallery directories.

Takes an absolute file location. If None the method will run through a CLI set
to get the correct file for in the right gallery folder.
"""
        removed = False

        if file_ is not None:
            if exists(file_):
                remove(file_)
                if not exists(file_):
                    self.json_update = True
                    self.clean()
                    rStr = "The following file was removed along with all it's"
                    rStr += f' sibling files\n{file_}'
                    print(f'{rStr}\n')
                    return True
                else:
                    print(f'Failed to remove the file:\n{file_}')
                    return False
            else:
                return False

        ask = 'In what gallery is the image you want to to remove located?'
        galy = self.ask_for_gallery(ask+'\n')

        ls = num_alpha_sort(listdir(galy['path']))

        imgs = [x for x in ls if x[0] != '.' and isfile(oJoin(galy['path'], x))]
        imgs = {x:y for x, y in enumerate(imgs, 1)}

        ask = '\nWhich image would you like to delete?\n'

        for imgNum, image in imgs.items():
            ask += f'  {imgNum}: {image}\n'

        ask += ' Enter number or image name:'
        
        rmFile = ''

        try:
            while True:
                message = input(f'{ask}\n> ')

                if message.isdigit():
                    rmFile = oJoin(galy['path'], imgs[int(message)])
                    if exists(rmFile):
                        remove(rmFile)
                        removed = not exists(rmFile)
                        break
                else:
                    rmFile = [x for x in imgs.values() if x == message]
                    if rmFile != []:
                        rmFile = oJoin(galy['path'], rmFile[0])
                        if exists(rmFile):
                            remove(rmFile)
                            removed = not exists(rmFile)
                            break

            if removed:
                self.json_update = True
                self.clean()
                rStr = "The following file was removed along with all it's"
                rStr += f' sibling files:\n  {rmFile}'
                print(f'{rStr}\n')
            else:
                print(f'Failed to remove the file:\n{rmFile}\n')

        except (KeyboardInterrupt, SystemExit):
            sExit('\nGoodbye!')


    def _rename(self, oldF=None, newF=None):
        """
This method will do the heavy lifting for the actual image/file renaming 
process.
"""
        newFile = oJoin(oSplit(oldF)[0], newF)

        if oldF is not None and newF is not None and not exists(newFile):

            file_pairs = ((oldF, newFile),)

            origin = oJoin(oSplit(oldF)[0], 'Original_Archive', oSplit(oldF)[1])
            archiv = oJoin(oSplit(oldF)[0], 'Original_Archive', oSplit(newF)[1])
            file_pairs = file_pairs + ((origin, archiv),)

            othumb = oJoin(oSplit(oldF)[0], 'thumbs', oSplit(oldF)[1])
            nthumb = oJoin(oSplit(oldF)[0], 'thumbs', oSplit(newF)[1])
            file_pairs = file_pairs + ((othumb, nthumb),)

            otext = oJoin(oSplit(oldF)[0],
                          'Images_Info_Text',
                          f'{splitext(oSplit(oldF)[1])[0]}.txt')

            ntext = oJoin(oSplit(oldF)[0],
                          'Images_Info_Text',
                          f'{splitext(oSplit(newF)[1])[0]}.txt')

            file_pairs = file_pairs + ((otext, ntext),)

            for pair in file_pairs:
                if exists(pair[0]) and not exists(pair[1]):
                    move(*pair)

            if exists(file_pairs[0][1]):
                print('File renamed successfully.')
                self.json_update = True
                self.clean()
            else:
                print('Failed to rename file.')
        else:

            msg = '\nCould not rename the files. \nMost likely the new name '
            msg += 'already exists in this gallery and has \nto be removed or'
            msg += ' renamed before this operation can take place.'
            print(msg)


    def rename_image(self):
        """
This method will walk the user through the process of renaming an image after
they have selected the gallery. This method will rename all sibling files in the
archive, thumbnail and text file folders.
"""
        ask = 'In what gallery is the image you want to to rename located?'
        galy = self.ask_for_gallery(ask+'\n')

        lst = num_alpha_sort(listdir(galy['path']))

        imgs = [x for x in lst if x[0] != '.' and isfile(oJoin(galy['path'], x))]
        imgs = {x:y for x, y in enumerate(imgs, 1)}

        ask = '\nWhich image would you like to rename?\n'

        for imgNum, image in imgs.items():
            ask += f'  {imgNum}: {image}\n'

        ask += ' Enter number or image name:'
        
        rnFile = ''

        try:
            while True:
                message = input(f'{ask}\n> ')
                if message.isdigit():
                    rnFile = oJoin(galy['path'], imgs[int(message)])
                    if exists(rnFile):
                        msg = input('Please enter a new name:\n> ')
                        if msg != '':
                            msg = splitext(msg)[0] + splitext(rnFile)[1]
                            self._rename(rnFile, msg)
                            break
                else:
                    rnFile = [x for x in imgs.values() if x == message]
                    if rnFile != []:
                        rnFile = oJoin(galy['path'], rnFile[0])
                        if exists(rnFile):
                            msg = input('Please enter a new name:\n> ')
                            if msg != '':
                                msg = splitext(msg)[0] + splitext(rnFile)[1]
                                self._rename(rnFile, msg)
                                break

        except (KeyboardInterrupt, SystemExit):
            sExit('Goodbye!')


    def set_start_image(self):
        """
This method will set the gallery's index image. The image that will represent 
the individual gallery. This method will run through a basic setup process.
"""
        ask = 'What gallery do you want to set the default image for?'
        galy = self.ask_for_gallery(ask+'\n')

        lst = num_alpha_sort(listdir(galy['path']))

        imgs = [x for x in lst if x[0] != '.' and isfile(oJoin(galy['path'], x))]
        imgs = {x:y for x, y in enumerate(imgs, 1)}

        ask = '\nWhich image would you like to use?\n'

        for imgNum, image in imgs.items():
            ask += f'  {imgNum}: {image}\n'

        ask += ' Enter number or image name:'
        
        deFile = ''

        try:
            while True:
                message = input(f'{ask}\n> ')
                if message.isdigit():
                    deFile = oJoin(galy['path'], imgs[int(message)])
                    if exists(deFile):
                        sFile = oSplit(deFile)
                        self.default_filenames(sFile[0])
                        fName = splitext(sFile[1])
                        print("Attempting to set the gallery's index image.")
                        self._rename(deFile, f'_{fName[0]}_{fName[1]}')
                        break
                else:
                    deFile = [x for x in imgs.values() if x == message]
                    if deFile != []:
                        deFile = oJoin(galy['path'], deFile[0])
                        if exists(deFile):
                            sFile = oSplit(deFile)
                            self.default_filenames(sFile[0])
                            fName = splitext(sFile[1])
                            print("Attempting to set the gallery's index image.")
                            self._rename(deFile, f'_{fName[0]}_{fName[1]}')
                            break

        except (KeyboardInterrupt, SystemExit):
            sExit('Goodbye!')


    def default_filenames(self, dir_):
        """
This method will look through all files in a directory and remove any preceding
and matching trailing underscores in a filename. The two underscores are what
this program uses to differentiate a gallery's representative image. This sets
all filenames to default."""
        for file_ in listdir(dir_):
            if file_[0] == '_' and splitext(file_)[0][-1] == '_':
                print("Renaming our old gallery's index file to it's default.")
                spFile = splitext(file_)
                self._rename(oJoin(dir_, file_), f'{spFile[0][1:-1]}{spFile[1]}')
                self.json_update = True


    def clean(self, verbose=False):
        """
This class will compare our gallery's parent directory's image files with their 
possible respective children directories images. If a file in the children's 
folders is found that's not supposed to exist because it doesn't have a parent
that children's files will be removed  
"""
        print('Cleaning ...')
        for path_, _, files_ in walk(self.base_dir):

            gal = {'origin': {'pth': oJoin(path_, 'Original_Archive')},
                   'thumbs': {'pth': oJoin(path_, 'thumbs')},
                   'text': {'pth': oJoin(path_, 'Images_Info_Text')}}

            isTopDir = bool([1, 1, 1] == [exists(x['pth']) for x in gal.values()])

            if isTopDir:
                gal['origin']['ls'] = listdir(gal['origin']['pth'])
                gal['origin']['ls'] = [x for x in gal['origin']['ls'] if x[0] != '.']

                gal['thumbs']['ls'] = listdir(gal['thumbs']['pth'])
                gal['thumbs']['ls'] = [x for x in gal['thumbs']['ls'] if x[0] != '.']

                gal['text']['ls'] = listdir(gal['text']['pth'])
                gal['text']['ls'] = [x for x in gal['text']['ls'] if x[0] != '.']

            for obj in gal.values():
                if isTopDir and isinstance(obj['ls'], list):
                    for file_ in obj['ls']:
                        fName = splitext(file_)
                        if fName[0] not in [splitext(x)[0] for x in files_]:
                            topDir = oSplit(path_)[1]
                            fullPath = oJoin(obj['pth'], file_)
                            if file_ != f'{topDir}.txt' and exists(fullPath):
                                remove(oJoin(obj['pth'], file_))
                                self.json_update = True
                                if verbose:
                                    print(f"removed file: {fullPath}")
                                        

class ImageSearch():
    """ 
This class will search a given directory recursively looking for images. It will
build a dictionary object with relative location values to this script.

When Images are found the images are backed up to the Original_Archive folder.
The images are then resized. 

Class will also check for possible existing settings in a json file. The actual
json file is built by his class's child class. 

The idea here is we just want to be able to set and forget our gallery's
settings. Once you have set the image_size and thumb_size variables you only 
have to pass them again if you would like to change sizes.

When sizes are modified the backup files are used. This is the maximum 
resolution of your image. Gallery images can only be equal in size or smaller.
"""

    def __init__(self, *args, **krgs):
        """\nUser can pass these keyword arguments.

    image_size=(int(width), int(height)
        # Expects a tuple of 2 integers representing width and height.

    thumb_size=(int(width), int(height)
        # Expects a tuple of 2 integers representing width and height.
"""
        self.base_dir = abspath(dirname(__file__))

        self._share = ()

        self.settings = {'imgs': ('.jpeg', '.jpg', '.png', '.gif', '.webp',
                                  '.svg'),
                         'img_size': self.get_json_size_for('img_size'),
                         'thumb_size': self.get_json_size_for('thumb_size')}

        if 'image_size' in krgs.keys():
            if isinstance(krgs['image_size'], tuple):
                width, height = krgs['image_size']

            if isinstance(krgs['image_size'], str) and 'x' in krgs['image_size']:
                width, height = krgs['image_size'].split('x')

            self.settings['img_size'] = (int(width), int(height))

        if 'thumb_size' in krgs.keys():
            if isinstance(krgs['thumb_size'], tuple):
                width, height = krgs['thumb_size']

            if isinstance(krgs['thumb_size'], str) and 'x' in krgs['thumb_size']:
                width, height = krgs['thumb_size'].split('x')

            self.settings['thumb_size'] = (int(width), int(height))

        self.files = {}

        self.img_search()


    def get_json_size_for(self, node):
        """\n
Method to get:set our default image and thumbnail sizes.
    Expects:
        dictionary key of either "img_size" or "thumb_size".
"""
        if node == 'img_size':
            default = (600, 400)

        if node == 'thumb_size':
            default = (75, 75)

        json_file = oJoin(self.base_dir, 'scripts', 'gallery.json')

        if not exists(json_file):
            return default

        with open(json_file, 'r') as file_:
            try:
                jData = json.loads(file_.read())
                return tuple(int(x) for x in jData['settings'][node])
            except json.JSONDecodeError:
                return default


    def img_search(self):
        """ 
This method will recursively search our base directory for images to be added to
our object.
"""
        for path, _, files in walk(self.base_dir):
            this_dir = oSplit(path)[1]
            look_for = ['thumbs', 'Original_Archive']
            if this_dir not in look_for and this_dir[0] != '.':
                for file_ in num_alpha_sort(files):
                    if splitext(file_)[1] in self.settings['imgs']:
                        if self._is_image(oJoin(path, file_)):
                            self._pic_node(relpath(oJoin(path, file_),
                                                   self.base_dir),
                                           files)

    
    def _pic_node(self, _img_path, _files):
        """ 
Here is the heavy lifter. This method will create our dictionary object unique 
to th image it has found.

    Example object.

            {
                "name": "Image Name",
                "path": "Images/Image Name.png",
                "thumb": {
                    "path": "Images/thumbs/Image Name.png",
                    "width": 50,
                    "height": 75
                },
                "width": 600,
                "height": 400,
                "info_text": "The Image Name is neet."
            }
     """

        ## get image folder name
        _img_folder = oSplit(oSplit(_img_path)[0])[1]

        ## get name from filename
        _name = splitext(oSplit(_img_path)[1])[0]

        ## build our dictionary object starting with what we know.
        _dict = {'name': _name,
                 'path': _img_path}

        ## open our image and make a backup to Original_archive directory.
        _img_backup = Image.open(oJoin(self.base_dir, _img_path))
        self.img_backup(_img_backup, _img_path)

        ## Open our image again to set it's new size. 
        _img = Image.open(oJoin(self.base_dir, _img_path))
        ## We also need it's mime type. Used when saving the image.
        _format = _img.format.lower()

        _wWidth, _wHeight = self.settings['img_size']

        if _wWidth != _img.width and _wHeight != _img.height:
            if '-json' not in self._share:
                self._share += ('-json',)

            ## User has asked to increase the size of their photos. Load
            ## up the archive and see what we cant do.
            archive = oJoin(oSplit(oJoin(self.base_dir, _img_path))[0],
                            'Original_Archive', oSplit(_img_path)[1])

            _img2 = Image.open(archive)

            if _wWidth > _img2.width and _wHeight > _img2.height:
                print(f'\t{oSplit(_img_path)[1]}\n\tSize error make one?')
            else:
                _img2 = image_resize(_img2, self.settings['img_size'])
                _img2.save(_img_path, _format, dpi=(144, 144))

            _img2.close()

        _img.close()

        ## Open out image to cerate our thumbnail image.
        _img = Image.open(oJoin(self.base_dir, _img_path))
        ## Create our thumbnail object
        _dict['thumb'] = self.create_thumb(_img_path, _format)

        ## set our width and height
        _dict['width'] = _img.width
        _dict['height'] = _img.height

        ## info/about text
        _dict['info_text'] = self.create_info(_img_path)

        _img.close()


        ## Now we have our completed gallery image node, add it to the files!
        if not _img_folder in self.files.keys():
            self.files[_img_folder] = {'images': [], 'info': '',
                                       'galleryIndex' : 0}

        self.files[_img_folder]['images'].append(_dict)


    def create_info(self, img_path):
        """ 
This method will find any text files that should be associated with this image. 
Each image can have one text file. It must share the same base name with 
differing dot extension of .txt.

The text files must be placed in the Images_Info_Text directory."""

        info_path = oJoin(oSplit(oJoin(self.base_dir, img_path))[0],
                          'Images_Info_Text')

        if not exists(info_path):
            mkdir(info_path)

        info_file = oJoin(info_path, splitext(oSplit(img_path)[1])[0]+'.txt')

        if exists(info_file):
            return relpath(info_file, self.base_dir)

        return ''


    def create_thumb(self, img_path, format_):
        """
This method will setup our thumbnail image. This will check the current thumb 
size and change it accordingly if it exists."""

        thumb_path = oJoin(oSplit(oJoin(self.base_dir, img_path))[0],
                           'thumbs')

        if not exists(thumb_path):
            mkdir(thumb_path)

        thumb = Image.open(img_path)

        thumb = image_resize(thumb, self.settings['thumb_size'])

        thumb_file = oJoin(thumb_path, oSplit(img_path)[1])
        # thumb_size = thumb.size

        if exists(thumb_file):

            thumb2 = Image.open(thumb_file)

            tWidth, tHeight = self.settings['thumb_size']

            if tWidth != thumb2.width and tHeight != thumb2.height:
                if '-json' not in self._share:
                    self._share += ('-json',)

                thumb.save(thumb_file, format_, dpi=(144, 144))
                thumb.close()
                thumb2.close()
            else:
                thumb.close()
                thumb2.close()

            thumb_dict = {'path': relpath(thumb_file, self.base_dir),
                          'width': thumb.width, 'height': thumb.height}

        else:

            thumb.save(thumb_file, format_, dpi=(144, 144))
            thumb.close()

            thumb_dict = {'path': relpath(thumb_file, self.base_dir),
                          'width': thumb.width, 'height': thumb.height}

        return thumb_dict


    def img_backup(self, img, img_path):
        """
This function makes a backup of the original image file to a folder called 
Original_Archive so it can be replaced with a hopefully smaller version to be 
used on the web. Also will be used for fullscreen image."""

        format_ = img.format.lower()

        archive = oJoin(oSplit(oJoin(self.base_dir, img_path))[0],
                        'Original_Archive')

        if not exists(archive):
            mkdir(archive)

        archive_file = oJoin(archive, oSplit(img_path)[1])

        if not exists(archive_file):
            img.save(archive_file, img.format.lower(), dpi=(144, 144))
        else:
            img2 = Image.open(archive_file)
           
            if img.size != img2.size:
                if img.size[0] > img2.size[0] or img.size[1] > img2.size[1]:
                    remove(img_path)
                    img.save(archive_file, format_, dpi=(144, 144))

            img2.close()

        img.close()


    def _is_image(self, img_file):
        """
This method checks if the img_file is a valid Image file testing it's dot 
extension matches the image mime type.
    
    returns boolean """
        try:
            im = Image.open(img_file)
        except OSError as e:
            ## The only reason that I think this would happen is if someone
            ## was trying to run this file on a folder that needs root 
            ## permission to write.
            raise e
        else:
            ext = splitext(img_file)[1][1:]
            _format = im.format.lower()
            if ext == _format or f'.{_format}' in self.settings['imgs'][:2]:
                im.close()
                return True
            im.close()
            return False


class mkGallery(ImageSearch):
    """
Surely this isn't any different than it's parent. Apple's something, something,
tree's.

This class will search a given directory recursively looking for images. It will
build a dictionary object with relative location values to this script.

When Images are found the images are backed up to the Original_Archive folder.
The images are then resized. 

Class will also check for possible existing settings in a json file. 

The idea here is we just want to be able to set and forget our gallery's
settings. Once you have set the image_size and thumb_size variables you only 
have to pass them again if you would like to change sizes.

When sizes are modified the backup files are used. This is the maximum 
resolution of your image. Gallery images can only be equal in size or smaller.

    Here's where things Change. Well really just gets added to.

From now on the class will handle building, loading and saving the gallery's
json configuration file. 

So after the directories have been scanned and the self.files object has been 
built we test if the known files are different than the json files info and if
it differs update the json's data.

The class method doc strings will provide further information about their json
file operations. 


"""

    def __init__(self, *args, **krgs):
        """
User can pass these keyword arguments.

    image_size=(int(width), int(height)
        # Expects a tuple of 2 integers representing width and height.

    thumb_size=(int(width), int(height)
        # Expects a tuple of 2 integers representing width and height.
"""

        ImageSearch.__init__(self, *args, **krgs)

        self.json_data = None
        self.get_json()

        if '-json-save-info' in args:
            ## okay the user says that the json file has info that should be 
            ## written to disk. Giver her the ol`d coaladge try: I never went. 
            self.json_save_infos()

        if '-json' in args or '-json' in self._share:
            print('Rebuilding the json file.')
            ## remake the json file with the saved gallery information
            ## Only effects the Images node. Settings will remain intact
            self.get_infos()
            self.set_json()

        ## we need to make the rest of our files if the user wants
        MakeWeb(*args)


    def json_save_infos(self):
        """ 
This method will save the individual image about info text files as well as all
the gallery's about text files from the data contained in the json file. 

This means you can edit the json file's about info directly. This method
absolutely has to be ran before any other action is preformed on the json file.
"""

        with open(oJoin(self.base_dir, 'scripts', 'gallery.json'), 'r') as file_:
            jData = json.loads(file_.read())

        for gallery in jData['gallerys']:
            # print(f"gallery: {gallery}, {jData['gallerys'][gallery]['info']}")
            if jData['gallerys'][gallery]['info'] != '':

                with open(oJoin(self.base_dir,
                                gallery,
                                'Images_Info_Text',
                                f'{gallery}.txt'), 'w') as file_:
                    file_.write(jData['gallerys'][gallery]['info'])

            for pic in jData['gallerys'][gallery]['images']:
                if pic['info_text'] != '':
                    with open(oJoin(self.base_dir,
                                    oSplit(pic['path'])[0],
                                    'Images_Info_Text',
                                    f"{pic['name']}.txt"), 'w') as file_:
                        file_.write(pic['info_text'])


    def get_infos(self):
        """
This method will look for and find all the individual image files and gallery's 
about info text. On finding a about info file it overwrites the data in the 
corresponding node in the json file. 

    ** important ** if you have data in the json file that Has not been saved do
       so before using this method.\n"""
        for folder in self.files:
            for key, img_ in enumerate(self.files[folder]['images']):
                info_file = img_['info_text']


                if img_['name'][0] == '_' and img_['name'][-1] == '_':
                    self.json_data['gallerys'][folder]['galleryIndex'] = key

                if info_file != '' and exists(oJoin(self.base_dir, info_file)):
                    with open(oJoin(self.base_dir, info_file), 'r') as _file:
                        info = _file.read()
                else:
                    info = ''

                self.json_data['gallerys'][folder]['images'][key]['info_text'] = info

            ## we need toget our gallerys information text if there is any.
            info_file = oJoin(self.base_dir, folder, 
                              'Images_Info_Text', f"{folder}.txt")
            if exists(info_file):
                with open(info_file, 'r') as file_:
                    self.json_data['gallerys'][folder]['info'] = file_.read()


    def get_json(self):
        """
This method will attempt to retrieve and load our json data. 

If the json data does not match our known files dictionary the [gallerys] node 
in the json file will be be replaced with our new gallery information.

If the json is broken, malformed or missing the file will be recreated.

This method will also tries to make sure the json always contains the correct 
image sizes. It does a good job.

          """

        json_file = oJoin(self.base_dir, 'scripts', 'gallery.json')

        json_default = {}
        json_default['settings'] = self.settings
        json_default['settings']['topDir'] = self.base_dir
        json_default['settings']['galleryOrder'] = False
        json_default['settings']['startGallery'] = False
        json_default['settings']['stageMultiplier'] = 0
        json_default['settings']['btnColUp'] = '#6495ED'
        json_default['settings']['btnColOver'] = '#A0D1FF'
        json_default['settings']['btnBgTop'] = '#2859b1'
        json_default['settings']['btnBgTopAlpha'] = 1
        json_default['settings']['btnBgBottom'] = '#201D1E'
        json_default['settings']['btnBgBottomAlpha'] = 1
        json_default['settings']['nowPlayColor'] = '#91C7F6'
        json_default['settings']['folder'] = {'folderBG': '#2859B1',
                                              'folderTab': '#6495ED',
                                              'tabShadow': '#00A4E0',
                                              'tabAccent': '#201D1E'}

        if not exists(oSplit(json_file)[0]):
            mkdir(oSplit(json_file)[0])

        try:
            with open(json_file, 'r') as oFile:
                try:
                    self.json_data = json.loads(oFile.read())

                    if self.json_data['gallerys'] != self.files:
                        self.json_data['gallerys'] = self.files

                except json.JSONDecodeError:
                    ## file is likely not a json file, malformed json or empty
                    ## rebuild it. 

                    print('Cant load. We will rebuild the json file.')

                    self._share += ('-json',)
                    self.json_data = json_default
                    self.json_data['gallerys'] = self.files

                    self.set_json()

                finally:
                    img_size = self.settings['img_size']
                    thumb_size = self.settings['thumb_size']
                    # print(f"image size: {img_size}\nthumb size: {thumb_size}")

                    if self.json_data['settings']['img_size'] != img_size:
                        self.json_data['settings']['img_size'] = img_size

                    if self.json_data['settings']['thumb_size'] != thumb_size:
                        self.json_data['settings']['thumb_size'] = thumb_size

        except FileNotFoundError:
            # there is no json file to read from so we will make it.
            self.json_data = json_default
            self.json_data['gallerys'] = self.files

            self.set_json()


    def set_json(self):
        """
This method here will write the json file. Currently it saves it to the 
mkGallery.py's top directory. Might need to cerate a "Scripts/" directory to 
keep all our loose scripts.
"""
        json_file = oJoin(self.base_dir, 'scripts', 'gallery.json')
        with open(json_file, 'w+') as file_:
            file_.write(json.dumps(self.json_data, indent=4))


def path_fix(path_, args=None):
    """
This function is for the future. I cant test yet because I don't want to boot 
into windows. I don't want to install php on windows. I don't want to test in 
windows! So I'm going to leave this here till there is a need for me to go to
windows.

This function attempts to normalize a file path that might need to be converted 
to a different operating systems naming convention, NT or POSIX. This should 
come in handy when writing the json file's paths in windows."""

    if args is not None and '-fix-windows-paths' in args:

        pth = PureWindowsPath(path_)

        if pth.is_absolute():
            return pth.as_posix().replace('C:/', '/')

        return pth.as_posix()

    if args is not None and '-fix-posix-paths' in args:

        pth = PureWindowsPath(path_)

        if pth.parts[0] == '\\':
            return 'C:\\\\' + '\\'.join(pth.parts[1:])

        return '\\' + '\\'.join(pth.parts)

    return path_



def image_resize(img, size):
    """This function will find reduce your image to fit."""
    rxWidth, rxHeight = img.size
    width, height = size

    if rxWidth > width:
        aspect = width / rxWidth
        rxWidth = round(aspect * rxWidth)
        rxHeight = round(aspect * rxHeight)

    if rxHeight > height:
        aspect = height / rxHeight
        rxWidth = round(aspect * rxWidth)
        rxHeight = round(aspect * rxHeight)

    return img.resize((rxWidth, rxHeight))


def num_alpha_sort(list_):
    """ 
This function will sort a list's values by splitting the file at ' ' and or at 
'_'. It then takes that value ans asks if it is a digit that can be converted 
into an integer. Two sorted lists are made from this logic. Strings that start 
with numeric values that should be numerically sorted and the other list that
should be sorted alphabetically. They are then added together and returned
"""
    sep = ('_', ' ')
    num = []
    alpha = []

    for sp in sep:
        num += sorted(set(x for x in list_ if x.split(sp)[0].isdigit() or x.split(sp)[0].isdigit()),
                      key=lambda x: int(x.split(sp)[0]) if x[0] != sp else int(x.split(sp)[1]))

    alpha = set(x for x in list_ if not x.split('_')[0].isdigit() and not x.split(' ')[0].isdigit())

    return num + sorted(alpha, key=lambda x: x.split('_')[1] if x[0] == '_' else x)


def need_help(args):
    """ Function to display the help text. """
    if True in (True for x in ('-h', '-help') if x in args):
        help_ = """
mkGallery.py, a program to build and maintain a html image gallery. 

This program will recursively search for image files in folders building a
multiple gallery file object. This object is then written to json. The json file 
is updated when any image changes names or is removed in the gallery's master
folder.

  Default image dimensions:

        Image width  :  600,
        Image height :  400,
        Thumb width  :   75,
        Thumb height :   75,

    Examples:

      Default:
        ## makes gallery with default sizes and all web files

        $ python3 mkGallery.py -all
    
      Arguments:
        ## makes user defined sized gallery and all web files

        $ python3 mkGallery.py image_size=700x400 thumb_size=85x85 -all


  HTML file building operations:

     -php                 Creates the gallery's php file. It is used to serve
                          the largest possible image when the user is in 
                          fullscreen.

     -html                Creates the gallery's html file. It will use the json
                          files settings to create the javascript's setting's
                          object.

     -js                  Creates a minified javascript file.

     -css                 Creates the very simple css file.

     -all                 Creates all the html files. 

  File operation helpers:

     -clean-loose-files   Compares gallery's main image folder and the archive,
                          thumbnail and text locations removing differences.

     -remove-image        Runs through a helper to find the right files in the 
                          right gallery directories and removes the files.

     -rename-image        Runs through a helper to find the right files in the
                          right gallery directories and renames them.  

     -set-start-image     Gallery's can have a image associated with it. The 
                          folder image in the web pages gallery chooser function
                          and this will run through a helper to setup.
"""
        sExit(help_)

def is_version(args):
    """ Prints this programs version number. """
    if True in (True for x in ('-v', '-version') if x in arg):
        sExit(__version__)


if __name__ == '__main__':

    ## Clean out the keyword argument variables and separators.
    arg = [x for x in argv[1:] if '=' not in x and ':' not in x]

    ## It's good to know when your suing outdated code. Here take this!
    is_version(arg)

    ## Be helpful.
    need_help(arg)

    ## Create the key value pairs from ':', or '=' separators.
    karg = {x.split(y, 1)[0]: x.split(y, 1)[1] for x in argv[1:] for y in ['=', ':'] if y in x}

    ## do cleaning stuff before any gallery operations.
    clean = Clean(*arg)

    if clean.json_update and '-json' not in arg:
        ## files have been updated and the json needs to be rebuilt.
        arg.append('-json')

    ## do gallery stuff
    mkGallery(*arg, **karg)
